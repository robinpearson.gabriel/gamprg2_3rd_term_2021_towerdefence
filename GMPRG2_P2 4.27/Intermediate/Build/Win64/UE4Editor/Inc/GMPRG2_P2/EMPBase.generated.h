// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_EMPBase_generated_h
#error "EMPBase.generated.h already included, missing '#pragma once' in EMPBase.h"
#endif
#define GMPRG2_P2_EMPBase_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEMPBase(); \
	friend struct Z_Construct_UClass_AEMPBase_Statics; \
public: \
	DECLARE_CLASS(AEMPBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AEMPBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAEMPBase(); \
	friend struct Z_Construct_UClass_AEMPBase_Statics; \
public: \
	DECLARE_CLASS(AEMPBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AEMPBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEMPBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEMPBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEMPBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEMPBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEMPBase(AEMPBase&&); \
	NO_API AEMPBase(const AEMPBase&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEMPBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEMPBase(AEMPBase&&); \
	NO_API AEMPBase(const AEMPBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEMPBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEMPBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEMPBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SlowEffect() { return STRUCT_OFFSET(AEMPBase, SlowEffect); } \
	FORCEINLINE static uint32 __PPO__slowIncrease() { return STRUCT_OFFSET(AEMPBase, slowIncrease); } \
	FORCEINLINE static uint32 __PPO__RangeIncreas() { return STRUCT_OFFSET(AEMPBase, RangeIncreas); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_12_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AEMPBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_EMPBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
