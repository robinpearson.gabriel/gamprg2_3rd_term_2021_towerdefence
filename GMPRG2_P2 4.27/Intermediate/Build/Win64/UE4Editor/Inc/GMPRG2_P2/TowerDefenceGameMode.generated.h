// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UWaveData;
#ifdef GMPRG2_P2_TowerDefenceGameMode_generated_h
#error "TowerDefenceGameMode.generated.h already included, missing '#pragma once' in TowerDefenceGameMode.h"
#endif
#define GMPRG2_P2_TowerDefenceGameMode_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBuy); \
	DECLARE_FUNCTION(execIncreaseMoney); \
	DECLARE_FUNCTION(execGetMoney); \
	DECLARE_FUNCTION(execenemyDied); \
	DECLARE_FUNCTION(execgetCurrentWaveIndex); \
	DECLARE_FUNCTION(execgettoTalWaves); \
	DECLARE_FUNCTION(execGetwaveProgression); \
	DECLARE_FUNCTION(execTakeDamage); \
	DECLARE_FUNCTION(execGetEnemySpawner); \
	DECLARE_FUNCTION(execWaveStart); \
	DECLARE_FUNCTION(execGetHP);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBuy); \
	DECLARE_FUNCTION(execIncreaseMoney); \
	DECLARE_FUNCTION(execGetMoney); \
	DECLARE_FUNCTION(execenemyDied); \
	DECLARE_FUNCTION(execgetCurrentWaveIndex); \
	DECLARE_FUNCTION(execgettoTalWaves); \
	DECLARE_FUNCTION(execGetwaveProgression); \
	DECLARE_FUNCTION(execTakeDamage); \
	DECLARE_FUNCTION(execGetEnemySpawner); \
	DECLARE_FUNCTION(execWaveStart); \
	DECLARE_FUNCTION(execGetHP);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_EVENT_PARMS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_CALLBACK_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerDefenceGameMode(); \
	friend struct Z_Construct_UClass_ATowerDefenceGameMode_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenceGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenceGameMode)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_INCLASS \
private: \
	static void StaticRegisterNativesATowerDefenceGameMode(); \
	friend struct Z_Construct_UClass_ATowerDefenceGameMode_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenceGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenceGameMode)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenceGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenceGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenceGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenceGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenceGameMode(ATowerDefenceGameMode&&); \
	NO_API ATowerDefenceGameMode(const ATowerDefenceGameMode&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenceGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenceGameMode(ATowerDefenceGameMode&&); \
	NO_API ATowerDefenceGameMode(const ATowerDefenceGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenceGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenceGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenceGameMode)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__wavedatas() { return STRUCT_OFFSET(ATowerDefenceGameMode, wavedatas); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_13_PROLOG \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_EVENT_PARMS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ATowerDefenceGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerDefenceGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
