// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/WaveData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaveData() {}
// Cross Module References
	GMPRG2_P2_API UScriptStruct* Z_Construct_UScriptStruct_FenemyToSpawn();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AEnemyBase_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_UWaveData_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_UWaveData();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
// End Cross Module References
class UScriptStruct* FenemyToSpawn::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GMPRG2_P2_API uint32 Get_Z_Construct_UScriptStruct_FenemyToSpawn_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FenemyToSpawn, Z_Construct_UPackage__Script_GMPRG2_P2(), TEXT("enemyToSpawn"), sizeof(FenemyToSpawn), Get_Z_Construct_UScriptStruct_FenemyToSpawn_Hash());
	}
	return Singleton;
}
template<> GMPRG2_P2_API UScriptStruct* StaticStruct<FenemyToSpawn>()
{
	return FenemyToSpawn::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FenemyToSpawn(FenemyToSpawn::StaticStruct, TEXT("/Script/GMPRG2_P2"), TEXT("enemyToSpawn"), false, nullptr, nullptr);
static struct FScriptStruct_GMPRG2_P2_StaticRegisterNativesFenemyToSpawn
{
	FScriptStruct_GMPRG2_P2_StaticRegisterNativesFenemyToSpawn()
	{
		UScriptStruct::DeferCppStructOps<FenemyToSpawn>(FName(TEXT("enemyToSpawn")));
	}
} ScriptStruct_GMPRG2_P2_StaticRegisterNativesFenemyToSpawn;
	struct Z_Construct_UScriptStruct_FenemyToSpawn_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_enemyClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_enemyClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_population_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_population;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Spawner_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Spawner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FenemyToSpawn_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FenemyToSpawn>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_enemyClass_MetaData[] = {
		{ "Category", "enemyToSpawn" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_enemyClass = { "enemyClass", nullptr, (EPropertyFlags)0x0014000000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FenemyToSpawn, enemyClass), Z_Construct_UClass_AEnemyBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_enemyClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_enemyClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_population_MetaData[] = {
		{ "Category", "enemyToSpawn" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_population = { "population", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FenemyToSpawn, population), METADATA_PARAMS(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_population_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_population_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_Spawner_MetaData[] = {
		{ "Category", "enemyToSpawn" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_Spawner = { "Spawner", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FenemyToSpawn, Spawner), METADATA_PARAMS(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_Spawner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_Spawner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FenemyToSpawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_enemyClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_population,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FenemyToSpawn_Statics::NewProp_Spawner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FenemyToSpawn_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
		nullptr,
		&NewStructOps,
		"enemyToSpawn",
		sizeof(FenemyToSpawn),
		alignof(FenemyToSpawn),
		Z_Construct_UScriptStruct_FenemyToSpawn_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FenemyToSpawn_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FenemyToSpawn()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FenemyToSpawn_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GMPRG2_P2();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("enemyToSpawn"), sizeof(FenemyToSpawn), Get_Z_Construct_UScriptStruct_FenemyToSpawn_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FenemyToSpawn_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FenemyToSpawn_Hash() { return 2335107498U; }
	DEFINE_FUNCTION(UWaveData::execGetDuration)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetDuration();
		P_NATIVE_END;
	}
	void UWaveData::StaticRegisterNativesUWaveData()
	{
		UClass* Class = UWaveData::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDuration", &UWaveData::execGetDuration },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWaveData_GetDuration_Statics
	{
		struct WaveData_eventGetDuration_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWaveData_GetDuration_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaveData_eventGetDuration_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWaveData_GetDuration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWaveData_GetDuration_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWaveData_GetDuration_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWaveData_GetDuration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWaveData, nullptr, "GetDuration", nullptr, nullptr, sizeof(WaveData_eventGetDuration_Parms), Z_Construct_UFunction_UWaveData_GetDuration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWaveData_GetDuration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWaveData_GetDuration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWaveData_GetDuration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWaveData_GetDuration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWaveData_GetDuration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWaveData_NoRegister()
	{
		return UWaveData::StaticClass();
	}
	struct Z_Construct_UClass_UWaveData_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Duration_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Duration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Spawns_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Spawns;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyHpScaling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EnemyHpScaling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DropRateScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DropRateScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveGold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaveGold;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_enemies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_enemies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_enemies;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaveData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWaveData_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWaveData_GetDuration, "GetDuration" }, // 4038808993
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "WaveData.h" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_Duration_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_Duration = { "Duration", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, Duration), METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_Duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_Duration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_Spawns_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_Spawns = { "Spawns", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, Spawns), METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_Spawns_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_Spawns_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_EnemyHpScaling_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_EnemyHpScaling = { "EnemyHpScaling", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, EnemyHpScaling), METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_EnemyHpScaling_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_EnemyHpScaling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_DropRateScale_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_DropRateScale = { "DropRateScale", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, DropRateScale), METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_DropRateScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_DropRateScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_WaveGold_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_WaveGold = { "WaveGold", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, WaveGold), METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_WaveGold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_WaveGold_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_enemies_Inner = { "enemies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FenemyToSpawn, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_enemies_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_enemies = { "enemies", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, enemies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_enemies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_enemies_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaveData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_Duration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_Spawns,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_EnemyHpScaling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_DropRateScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_WaveGold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_enemies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_enemies,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaveData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaveData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaveData_Statics::ClassParams = {
		&UWaveData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UWaveData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaveData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaveData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaveData, 3520566353);
	template<> GMPRG2_P2_API UClass* StaticClass<UWaveData>()
	{
		return UWaveData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaveData(Z_Construct_UClass_UWaveData, &UWaveData::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("UWaveData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaveData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
