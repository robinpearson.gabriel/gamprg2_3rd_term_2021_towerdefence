// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/BuildManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuildManager() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_ABuildManager_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ABuildManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	GMPRG2_P2_API UClass* Z_Construct_UClass_UTowerData_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AGhostTower_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ABuildManager::execcheckTowers)
	{
		P_GET_OBJECT(AActor,Z_Param_actor);
		P_GET_STRUCT(FVector,Z_Param_pos);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->checkTowers(Z_Param_actor,Z_Param_pos);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABuildManager::execOpenTowerHUD)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OpenTowerHUD();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABuildManager::execEndChecking)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndChecking();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABuildManager::execCheckNode)
	{
		P_GET_OBJECT(AActor,Z_Param_actor);
		P_GET_STRUCT(FVector,Z_Param_pos);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CheckNode(Z_Param_actor,Z_Param_pos);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABuildManager::execSpawnGhost)
	{
		P_GET_OBJECT(UTowerData,Z_Param_data);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnGhost(Z_Param_data);
		P_NATIVE_END;
	}
	void ABuildManager::StaticRegisterNativesABuildManager()
	{
		UClass* Class = ABuildManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CheckNode", &ABuildManager::execCheckNode },
			{ "checkTowers", &ABuildManager::execcheckTowers },
			{ "EndChecking", &ABuildManager::execEndChecking },
			{ "OpenTowerHUD", &ABuildManager::execOpenTowerHUD },
			{ "SpawnGhost", &ABuildManager::execSpawnGhost },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABuildManager_CheckNode_Statics
	{
		struct BuildManager_eventCheckNode_Parms
		{
			AActor* actor;
			FVector pos;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_actor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_pos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABuildManager_CheckNode_Statics::NewProp_actor = { "actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuildManager_eventCheckNode_Parms, actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABuildManager_CheckNode_Statics::NewProp_pos = { "pos", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuildManager_eventCheckNode_Parms, pos), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABuildManager_CheckNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuildManager_CheckNode_Statics::NewProp_actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuildManager_CheckNode_Statics::NewProp_pos,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuildManager_CheckNode_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuildManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuildManager_CheckNode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuildManager, nullptr, "CheckNode", nullptr, nullptr, sizeof(BuildManager_eventCheckNode_Parms), Z_Construct_UFunction_ABuildManager_CheckNode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_CheckNode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00880401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuildManager_CheckNode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_CheckNode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuildManager_CheckNode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuildManager_CheckNode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABuildManager_checkTowers_Statics
	{
		struct BuildManager_eventcheckTowers_Parms
		{
			AActor* actor;
			FVector pos;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_actor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_pos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABuildManager_checkTowers_Statics::NewProp_actor = { "actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuildManager_eventcheckTowers_Parms, actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABuildManager_checkTowers_Statics::NewProp_pos = { "pos", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuildManager_eventcheckTowers_Parms, pos), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABuildManager_checkTowers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuildManager_checkTowers_Statics::NewProp_actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuildManager_checkTowers_Statics::NewProp_pos,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuildManager_checkTowers_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuildManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuildManager_checkTowers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuildManager, nullptr, "checkTowers", nullptr, nullptr, sizeof(BuildManager_eventcheckTowers_Parms), Z_Construct_UFunction_ABuildManager_checkTowers_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_checkTowers_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00880401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuildManager_checkTowers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_checkTowers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuildManager_checkTowers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuildManager_checkTowers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABuildManager_EndChecking_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuildManager_EndChecking_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuildManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuildManager_EndChecking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuildManager, nullptr, "EndChecking", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuildManager_EndChecking_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_EndChecking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuildManager_EndChecking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuildManager_EndChecking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABuildManager_OpenTowerHUD_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuildManager_OpenTowerHUD_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuildManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuildManager_OpenTowerHUD_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuildManager, nullptr, "OpenTowerHUD", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuildManager_OpenTowerHUD_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_OpenTowerHUD_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuildManager_OpenTowerHUD()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuildManager_OpenTowerHUD_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics
	{
		struct BuildManager_eventSpawnGhost_Parms
		{
			UTowerData* data;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuildManager_eventSpawnGhost_Parms, data), Z_Construct_UClass_UTowerData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::NewProp_data,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuildManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuildManager, nullptr, "SpawnGhost", nullptr, nullptr, sizeof(BuildManager_eventSpawnGhost_Parms), Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuildManager_SpawnGhost()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuildManager_SpawnGhost_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABuildManager_NoRegister()
	{
		return ABuildManager::StaticClass();
	}
	struct Z_Construct_UClass_ABuildManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GhostTowerPrefab_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_GhostTowerPrefab;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABuildManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABuildManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABuildManager_CheckNode, "CheckNode" }, // 2766146185
		{ &Z_Construct_UFunction_ABuildManager_checkTowers, "checkTowers" }, // 2602233018
		{ &Z_Construct_UFunction_ABuildManager_EndChecking, "EndChecking" }, // 1291800944
		{ &Z_Construct_UFunction_ABuildManager_OpenTowerHUD, "OpenTowerHUD" }, // 265055207
		{ &Z_Construct_UFunction_ABuildManager_SpawnGhost, "SpawnGhost" }, // 2548399208
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuildManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BuildManager.h" },
		{ "ModuleRelativePath", "BuildManager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuildManager_Statics::NewProp_GhostTowerPrefab_MetaData[] = {
		{ "Category", "BuildManager" },
		{ "ModuleRelativePath", "BuildManager.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ABuildManager_Statics::NewProp_GhostTowerPrefab = { "GhostTowerPrefab", nullptr, (EPropertyFlags)0x0024080000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABuildManager, GhostTowerPrefab), Z_Construct_UClass_AGhostTower_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ABuildManager_Statics::NewProp_GhostTowerPrefab_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABuildManager_Statics::NewProp_GhostTowerPrefab_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABuildManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABuildManager_Statics::NewProp_GhostTowerPrefab,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABuildManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABuildManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABuildManager_Statics::ClassParams = {
		&ABuildManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABuildManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABuildManager_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABuildManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABuildManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABuildManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABuildManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABuildManager, 2150654063);
	template<> GMPRG2_P2_API UClass* StaticClass<ABuildManager>()
	{
		return ABuildManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABuildManager(Z_Construct_UClass_ABuildManager, &ABuildManager::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("ABuildManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABuildManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
