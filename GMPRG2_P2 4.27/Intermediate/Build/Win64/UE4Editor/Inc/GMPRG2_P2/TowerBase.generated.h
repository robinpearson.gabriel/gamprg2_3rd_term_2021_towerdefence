// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GMPRG2_P2_TowerBase_generated_h
#error "TowerBase.generated.h already included, missing '#pragma once' in TowerBase.h"
#endif
#define GMPRG2_P2_TowerBase_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_RPC_WRAPPERS \
	virtual void Fire_Implementation(AActor* enemy); \
 \
	DECLARE_FUNCTION(execGetCanUpgrade); \
	DECLARE_FUNCTION(execUpgrade); \
	DECLARE_FUNCTION(execgetUpgradeCost); \
	DECLARE_FUNCTION(execSetInformation); \
	DECLARE_FUNCTION(execSellTower); \
	DECLARE_FUNCTION(execOnActorExit); \
	DECLARE_FUNCTION(execOnActorOverlap); \
	DECLARE_FUNCTION(execFire);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCanUpgrade); \
	DECLARE_FUNCTION(execUpgrade); \
	DECLARE_FUNCTION(execgetUpgradeCost); \
	DECLARE_FUNCTION(execSetInformation); \
	DECLARE_FUNCTION(execSellTower); \
	DECLARE_FUNCTION(execOnActorExit); \
	DECLARE_FUNCTION(execOnActorOverlap); \
	DECLARE_FUNCTION(execFire);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_EVENT_PARMS \
	struct TowerBase_eventFire_Parms \
	{ \
		AActor* enemy; \
	}; \
	struct TowerBase_eventgetTargets_Parms \
	{ \
		AActor* element; \
	}; \
	struct TowerBase_eventOPenCloseHUD_Parms \
	{ \
		bool Open; \
	};


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_CALLBACK_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerBase(); \
	friend struct Z_Construct_UClass_ATowerBase_Statics; \
public: \
	DECLARE_CLASS(ATowerBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATowerBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesATowerBase(); \
	friend struct Z_Construct_UClass_ATowerBase_Statics; \
public: \
	DECLARE_CLASS(ATowerBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATowerBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerBase(ATowerBase&&); \
	NO_API ATowerBase(const ATowerBase&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerBase(ATowerBase&&); \
	NO_API ATowerBase(const ATowerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATowerBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__root() { return STRUCT_OFFSET(ATowerBase, root); } \
	FORCEINLINE static uint32 __PPO__Radius() { return STRUCT_OFFSET(ATowerBase, Radius); } \
	FORCEINLINE static uint32 __PPO__collider() { return STRUCT_OFFSET(ATowerBase, collider); } \
	FORCEINLINE static uint32 __PPO__Gun() { return STRUCT_OFFSET(ATowerBase, Gun); } \
	FORCEINLINE static uint32 __PPO__GunBase() { return STRUCT_OFFSET(ATowerBase, GunBase); } \
	FORCEINLINE static uint32 __PPO__targets() { return STRUCT_OFFSET(ATowerBase, targets); } \
	FORCEINLINE static uint32 __PPO__Name() { return STRUCT_OFFSET(ATowerBase, Name); } \
	FORCEINLINE static uint32 __PPO__Stats() { return STRUCT_OFFSET(ATowerBase, Stats); } \
	FORCEINLINE static uint32 __PPO__SellCost() { return STRUCT_OFFSET(ATowerBase, SellCost); } \
	FORCEINLINE static uint32 __PPO__level() { return STRUCT_OFFSET(ATowerBase, level); } \
	FORCEINLINE static uint32 __PPO__Cost() { return STRUCT_OFFSET(ATowerBase, Cost); } \
	FORCEINLINE static uint32 __PPO__meshes() { return STRUCT_OFFSET(ATowerBase, meshes); } \
	FORCEINLINE static uint32 __PPO__target() { return STRUCT_OFFSET(ATowerBase, target); } \
	FORCEINLINE static uint32 __PPO__pivot() { return STRUCT_OFFSET(ATowerBase, pivot); } \
	FORCEINLINE static uint32 __PPO__spawnpoint() { return STRUCT_OFFSET(ATowerBase, spawnpoint); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_10_PROLOG \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_EVENT_PARMS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ATowerBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
