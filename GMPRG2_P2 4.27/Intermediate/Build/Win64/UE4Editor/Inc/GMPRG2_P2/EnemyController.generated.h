// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_EnemyController_generated_h
#error "EnemyController.generated.h already included, missing '#pragma once' in EnemyController.h"
#endif
#define GMPRG2_P2_EnemyController_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyController(); \
	friend struct Z_Construct_UClass_AEnemyController_Statics; \
public: \
	DECLARE_CLASS(AEnemyController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AEnemyController)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyController(); \
	friend struct Z_Construct_UClass_AEnemyController_Statics; \
public: \
	DECLARE_CLASS(AEnemyController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AEnemyController)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyController(AEnemyController&&); \
	NO_API AEnemyController(const AEnemyController&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyController(AEnemyController&&); \
	NO_API AEnemyController(const AEnemyController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyController)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_PRIVATE_PROPERTY_OFFSET
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_13_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AEnemyController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
