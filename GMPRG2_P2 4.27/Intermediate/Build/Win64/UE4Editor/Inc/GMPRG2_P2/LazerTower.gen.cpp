// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/LazerTower.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLazerTower() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_ALazerTower_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ALazerTower();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static FName NAME_ALazerTower_DrawLine = FName(TEXT("DrawLine"));
	void ALazerTower::DrawLine(FVector Startpos, FVector end)
	{
		LazerTower_eventDrawLine_Parms Parms;
		Parms.Startpos=Startpos;
		Parms.end=end;
		ProcessEvent(FindFunctionChecked(NAME_ALazerTower_DrawLine),&Parms);
	}
	void ALazerTower::StaticRegisterNativesALazerTower()
	{
	}
	struct Z_Construct_UFunction_ALazerTower_DrawLine_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Startpos;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_end;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ALazerTower_DrawLine_Statics::NewProp_Startpos = { "Startpos", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LazerTower_eventDrawLine_Parms, Startpos), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ALazerTower_DrawLine_Statics::NewProp_end = { "end", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LazerTower_eventDrawLine_Parms, end), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALazerTower_DrawLine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALazerTower_DrawLine_Statics::NewProp_Startpos,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALazerTower_DrawLine_Statics::NewProp_end,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALazerTower_DrawLine_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "LazerTower.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALazerTower_DrawLine_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALazerTower, nullptr, "DrawLine", nullptr, nullptr, sizeof(LazerTower_eventDrawLine_Parms), Z_Construct_UFunction_ALazerTower_DrawLine_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALazerTower_DrawLine_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08880800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALazerTower_DrawLine_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALazerTower_DrawLine_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALazerTower_DrawLine()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALazerTower_DrawLine_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ALazerTower_NoRegister()
	{
		return ALazerTower::StaticClass();
	}
	struct Z_Construct_UClass_ALazerTower_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_damage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_damage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_duration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_duration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fireRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fireRate;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DamageIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DamageIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DamageIncrease;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fireRateIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fireRateIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_fireRateIncrease;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALazerTower_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATowerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ALazerTower_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ALazerTower_DrawLine, "DrawLine" }, // 117199407
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALazerTower_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "LazerTower.h" },
		{ "ModuleRelativePath", "LazerTower.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALazerTower_Statics::NewProp_damage_MetaData[] = {
		{ "Category", "LazerTower" },
		{ "ModuleRelativePath", "LazerTower.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALazerTower_Statics::NewProp_damage = { "damage", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALazerTower, damage), METADATA_PARAMS(Z_Construct_UClass_ALazerTower_Statics::NewProp_damage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALazerTower_Statics::NewProp_damage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALazerTower_Statics::NewProp_duration_MetaData[] = {
		{ "Category", "LazerTower" },
		{ "ModuleRelativePath", "LazerTower.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALazerTower_Statics::NewProp_duration = { "duration", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALazerTower, duration), METADATA_PARAMS(Z_Construct_UClass_ALazerTower_Statics::NewProp_duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALazerTower_Statics::NewProp_duration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRate_MetaData[] = {
		{ "Category", "LazerTower" },
		{ "ModuleRelativePath", "LazerTower.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRate = { "fireRate", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALazerTower, fireRate), METADATA_PARAMS(Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRate_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALazerTower_Statics::NewProp_DamageIncrease_Inner = { "DamageIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALazerTower_Statics::NewProp_DamageIncrease_MetaData[] = {
		{ "Category", "LazerTower" },
		{ "ModuleRelativePath", "LazerTower.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ALazerTower_Statics::NewProp_DamageIncrease = { "DamageIncrease", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALazerTower, DamageIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ALazerTower_Statics::NewProp_DamageIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALazerTower_Statics::NewProp_DamageIncrease_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRateIncrease_Inner = { "fireRateIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRateIncrease_MetaData[] = {
		{ "Category", "LazerTower" },
		{ "ModuleRelativePath", "LazerTower.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRateIncrease = { "fireRateIncrease", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALazerTower, fireRateIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRateIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRateIncrease_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALazerTower_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALazerTower_Statics::NewProp_damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALazerTower_Statics::NewProp_duration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALazerTower_Statics::NewProp_DamageIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALazerTower_Statics::NewProp_DamageIncrease,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRateIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALazerTower_Statics::NewProp_fireRateIncrease,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALazerTower_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALazerTower>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALazerTower_Statics::ClassParams = {
		&ALazerTower::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ALazerTower_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ALazerTower_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALazerTower_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALazerTower_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALazerTower()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALazerTower_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALazerTower, 3391608324);
	template<> GMPRG2_P2_API UClass* StaticClass<ALazerTower>()
	{
		return ALazerTower::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALazerTower(Z_Construct_UClass_ALazerTower, &ALazerTower::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("ALazerTower"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALazerTower);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
