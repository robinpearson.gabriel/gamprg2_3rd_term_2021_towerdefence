// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_EnemyBase_generated_h
#error "EnemyBase.generated.h already included, missing '#pragma once' in EnemyBase.h"
#endif
#define GMPRG2_P2_EnemyBase_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_9_DELEGATE \
struct _Script_GMPRG2_P2_eventPlayerKilledSignaiture_Parms \
{ \
	int32 drop; \
}; \
static inline void FPlayerKilledSignaiture_DelegateWrapper(const FMulticastScriptDelegate& PlayerKilledSignaiture, int32 drop) \
{ \
	_Script_GMPRG2_P2_eventPlayerKilledSignaiture_Parms Parms; \
	Parms.drop=drop; \
	PlayerKilledSignaiture.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_8_DELEGATE \
static inline void FPlayerDieSignaiture_DelegateWrapper(const FMulticastScriptDelegate& PlayerDieSignaiture) \
{ \
	PlayerDieSignaiture.ProcessMulticastDelegate<UObject>(NULL); \
}


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execgetHealthPercentage); \
	DECLARE_FUNCTION(execTakeDamage); \
	DECLARE_FUNCTION(execgetHp); \
	DECLARE_FUNCTION(execkilled); \
	DECLARE_FUNCTION(execGetbaseSpeed); \
	DECLARE_FUNCTION(execGetBaseHP); \
	DECLARE_FUNCTION(execdie);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execgetHealthPercentage); \
	DECLARE_FUNCTION(execTakeDamage); \
	DECLARE_FUNCTION(execgetHp); \
	DECLARE_FUNCTION(execkilled); \
	DECLARE_FUNCTION(execGetbaseSpeed); \
	DECLARE_FUNCTION(execGetBaseHP); \
	DECLARE_FUNCTION(execdie);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_EVENT_PARMS \
	struct EnemyBase_eventAddSlow_Parms \
	{ \
		float slowRate; \
	};


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_CALLBACK_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyBase(); \
	friend struct Z_Construct_UClass_AEnemyBase_Statics; \
public: \
	DECLARE_CLASS(AEnemyBase, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AEnemyBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyBase(); \
	friend struct Z_Construct_UClass_AEnemyBase_Statics; \
public: \
	DECLARE_CLASS(AEnemyBase, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AEnemyBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyBase(AEnemyBase&&); \
	NO_API AEnemyBase(const AEnemyBase&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyBase(AEnemyBase&&); \
	NO_API AEnemyBase(const AEnemyBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemyBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BaseSpeed() { return STRUCT_OFFSET(AEnemyBase, BaseSpeed); } \
	FORCEINLINE static uint32 __PPO__Drop() { return STRUCT_OFFSET(AEnemyBase, Drop); } \
	FORCEINLINE static uint32 __PPO__BaseHP() { return STRUCT_OFFSET(AEnemyBase, BaseHP); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_10_PROLOG \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_EVENT_PARMS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AEnemyBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
