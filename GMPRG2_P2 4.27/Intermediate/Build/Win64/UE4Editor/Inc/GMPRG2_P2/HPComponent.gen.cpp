// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/HPComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHPComponent() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_UHPComponent_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_UHPComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	DEFINE_FUNCTION(UHPComponent::execGetHPPercentage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetHPPercentage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHPComponent::execGetHP)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetHP();
		P_NATIVE_END;
	}
	void UHPComponent::StaticRegisterNativesUHPComponent()
	{
		UClass* Class = UHPComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetHP", &UHPComponent::execGetHP },
			{ "GetHPPercentage", &UHPComponent::execGetHPPercentage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UHPComponent_GetHP_Statics
	{
		struct HPComponent_eventGetHP_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UHPComponent_GetHP_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HPComponent_eventGetHP_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHPComponent_GetHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHPComponent_GetHP_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHPComponent_GetHP_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "HPComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHPComponent_GetHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHPComponent, nullptr, "GetHP", nullptr, nullptr, sizeof(HPComponent_eventGetHP_Parms), Z_Construct_UFunction_UHPComponent_GetHP_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHPComponent_GetHP_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHPComponent_GetHP_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHPComponent_GetHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHPComponent_GetHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHPComponent_GetHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics
	{
		struct HPComponent_eventGetHPPercentage_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HPComponent_eventGetHPPercentage_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "HPComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHPComponent, nullptr, "GetHPPercentage", nullptr, nullptr, sizeof(HPComponent_eventGetHPPercentage_Parms), Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHPComponent_GetHPPercentage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHPComponent_GetHPPercentage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UHPComponent_NoRegister()
	{
		return UHPComponent::StaticClass();
	}
	struct Z_Construct_UClass_UHPComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHPComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UHPComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UHPComponent_GetHP, "GetHP" }, // 2264525807
		{ &Z_Construct_UFunction_UHPComponent_GetHPPercentage, "GetHPPercentage" }, // 4052301246
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHPComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "HPComponent.h" },
		{ "ModuleRelativePath", "HPComponent.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHPComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHPComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHPComponent_Statics::ClassParams = {
		&UHPComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UHPComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHPComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHPComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHPComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHPComponent, 2427322200);
	template<> GMPRG2_P2_API UClass* StaticClass<UHPComponent>()
	{
		return UHPComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHPComponent(Z_Construct_UClass_UHPComponent, &UHPComponent::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("UHPComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHPComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
