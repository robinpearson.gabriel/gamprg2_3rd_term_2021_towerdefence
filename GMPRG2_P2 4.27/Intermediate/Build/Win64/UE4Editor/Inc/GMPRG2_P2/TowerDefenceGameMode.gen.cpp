// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/TowerDefenceGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerDefenceGameMode() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerDefenceGameMode_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerDefenceGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_UWaveData_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ATowerDefenceGameMode::execBuy)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Decrease);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Buy(Z_Param_Decrease);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execIncreaseMoney)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_increase);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncreaseMoney(Z_Param_increase);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execGetMoney)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetMoney();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execenemyDied)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->enemyDied();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execgetCurrentWaveIndex)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->getCurrentWaveIndex();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execgettoTalWaves)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->gettoTalWaves();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execGetwaveProgression)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetwaveProgression();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execTakeDamage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TakeDamage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execGetEnemySpawner)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_index);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=P_THIS->GetEnemySpawner(Z_Param_index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execWaveStart)
	{
		P_GET_OBJECT(UWaveData,Z_Param_wavedata);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WaveStart(Z_Param_wavedata);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenceGameMode::execGetHP)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetHP();
		P_NATIVE_END;
	}
	static FName NAME_ATowerDefenceGameMode_Lose = FName(TEXT("Lose"));
	void ATowerDefenceGameMode::Lose()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATowerDefenceGameMode_Lose),NULL);
	}
	static FName NAME_ATowerDefenceGameMode_Win = FName(TEXT("Win"));
	void ATowerDefenceGameMode::Win()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATowerDefenceGameMode_Win),NULL);
	}
	void ATowerDefenceGameMode::StaticRegisterNativesATowerDefenceGameMode()
	{
		UClass* Class = ATowerDefenceGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Buy", &ATowerDefenceGameMode::execBuy },
			{ "enemyDied", &ATowerDefenceGameMode::execenemyDied },
			{ "getCurrentWaveIndex", &ATowerDefenceGameMode::execgetCurrentWaveIndex },
			{ "GetEnemySpawner", &ATowerDefenceGameMode::execGetEnemySpawner },
			{ "GetHP", &ATowerDefenceGameMode::execGetHP },
			{ "GetMoney", &ATowerDefenceGameMode::execGetMoney },
			{ "gettoTalWaves", &ATowerDefenceGameMode::execgettoTalWaves },
			{ "GetwaveProgression", &ATowerDefenceGameMode::execGetwaveProgression },
			{ "IncreaseMoney", &ATowerDefenceGameMode::execIncreaseMoney },
			{ "TakeDamage", &ATowerDefenceGameMode::execTakeDamage },
			{ "WaveStart", &ATowerDefenceGameMode::execWaveStart },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics
	{
		struct TowerDefenceGameMode_eventBuy_Parms
		{
			int32 Decrease;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Decrease;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::NewProp_Decrease = { "Decrease", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventBuy_Parms, Decrease), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::NewProp_Decrease,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "Buy", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventBuy_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_Buy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_Buy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "enemyDied", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics
	{
		struct TowerDefenceGameMode_eventgetCurrentWaveIndex_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventgetCurrentWaveIndex_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "getCurrentWaveIndex", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventgetCurrentWaveIndex_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics
	{
		struct TowerDefenceGameMode_eventGetEnemySpawner_Parms
		{
			int32 index;
			AActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::NewProp_index = { "index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventGetEnemySpawner_Parms, index), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventGetEnemySpawner_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::NewProp_index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "GetEnemySpawner", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventGetEnemySpawner_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics
	{
		struct TowerDefenceGameMode_eventGetHP_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventGetHP_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "GetHP", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventGetHP_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_GetHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_GetHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics
	{
		struct TowerDefenceGameMode_eventGetMoney_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventGetMoney_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "GetMoney", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventGetMoney_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics
	{
		struct TowerDefenceGameMode_eventgettoTalWaves_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventgettoTalWaves_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "gettoTalWaves", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventgettoTalWaves_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics
	{
		struct TowerDefenceGameMode_eventGetwaveProgression_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventGetwaveProgression_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "GetwaveProgression", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventGetwaveProgression_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics
	{
		struct TowerDefenceGameMode_eventIncreaseMoney_Parms
		{
			int32 increase;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_increase;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::NewProp_increase = { "increase", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventIncreaseMoney_Parms, increase), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::NewProp_increase,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "IncreaseMoney", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventIncreaseMoney_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_Lose_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_Lose_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_Lose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "Lose", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_Lose_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_Lose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_Lose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_Lose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//void SetSpawnPoints();\n" },
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
		{ "ToolTip", "void SetSpawnPoints();" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "TakeDamage", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics
	{
		struct TowerDefenceGameMode_eventWaveStart_Parms
		{
			UWaveData* wavedata;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wavedata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::NewProp_wavedata = { "wavedata", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenceGameMode_eventWaveStart_Parms, wavedata), Z_Construct_UClass_UWaveData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::NewProp_wavedata,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "WaveStart", nullptr, nullptr, sizeof(TowerDefenceGameMode_eventWaveStart_Parms), Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenceGameMode_Win_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenceGameMode_Win_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenceGameMode_Win_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenceGameMode, nullptr, "Win", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenceGameMode_Win_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenceGameMode_Win_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenceGameMode_Win()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenceGameMode_Win_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATowerDefenceGameMode_NoRegister()
	{
		return ATowerDefenceGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATowerDefenceGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wavedatas_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wavedatas_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_wavedatas;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATowerDefenceGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATowerDefenceGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_Buy, "Buy" }, // 3538871016
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_enemyDied, "enemyDied" }, // 2879830532
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_getCurrentWaveIndex, "getCurrentWaveIndex" }, // 2919917102
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_GetEnemySpawner, "GetEnemySpawner" }, // 4031774532
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_GetHP, "GetHP" }, // 1243284542
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_GetMoney, "GetMoney" }, // 1796961535
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_gettoTalWaves, "gettoTalWaves" }, // 4193376567
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_GetwaveProgression, "GetwaveProgression" }, // 3109995105
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_IncreaseMoney, "IncreaseMoney" }, // 583596025
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_Lose, "Lose" }, // 2952077303
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_TakeDamage, "TakeDamage" }, // 1874507747
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_WaveStart, "WaveStart" }, // 3416942732
		{ &Z_Construct_UFunction_ATowerDefenceGameMode_Win, "Win" }, // 4018096785
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerDefenceGameMode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TowerDefenceGameMode.h" },
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATowerDefenceGameMode_Statics::NewProp_wavedatas_Inner = { "wavedatas", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UWaveData_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerDefenceGameMode_Statics::NewProp_wavedatas_MetaData[] = {
		{ "Category", "TowerDefenceGameMode" },
		{ "ModuleRelativePath", "TowerDefenceGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ATowerDefenceGameMode_Statics::NewProp_wavedatas = { "wavedatas", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerDefenceGameMode, wavedatas), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ATowerDefenceGameMode_Statics::NewProp_wavedatas_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenceGameMode_Statics::NewProp_wavedatas_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATowerDefenceGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerDefenceGameMode_Statics::NewProp_wavedatas_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerDefenceGameMode_Statics::NewProp_wavedatas,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATowerDefenceGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATowerDefenceGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATowerDefenceGameMode_Statics::ClassParams = {
		&ATowerDefenceGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATowerDefenceGameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenceGameMode_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATowerDefenceGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenceGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATowerDefenceGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATowerDefenceGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATowerDefenceGameMode, 2497780609);
	template<> GMPRG2_P2_API UClass* StaticClass<ATowerDefenceGameMode>()
	{
		return ATowerDefenceGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATowerDefenceGameMode(Z_Construct_UClass_ATowerDefenceGameMode, &ATowerDefenceGameMode::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("ATowerDefenceGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATowerDefenceGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
