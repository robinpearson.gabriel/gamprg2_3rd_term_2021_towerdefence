// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
struct FVector;
class UTowerData;
#ifdef GMPRG2_P2_BuildManager_generated_h
#error "BuildManager.generated.h already included, missing '#pragma once' in BuildManager.h"
#endif
#define GMPRG2_P2_BuildManager_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execcheckTowers); \
	DECLARE_FUNCTION(execOpenTowerHUD); \
	DECLARE_FUNCTION(execEndChecking); \
	DECLARE_FUNCTION(execCheckNode); \
	DECLARE_FUNCTION(execSpawnGhost);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execcheckTowers); \
	DECLARE_FUNCTION(execOpenTowerHUD); \
	DECLARE_FUNCTION(execEndChecking); \
	DECLARE_FUNCTION(execCheckNode); \
	DECLARE_FUNCTION(execSpawnGhost);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABuildManager(); \
	friend struct Z_Construct_UClass_ABuildManager_Statics; \
public: \
	DECLARE_CLASS(ABuildManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ABuildManager)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABuildManager(); \
	friend struct Z_Construct_UClass_ABuildManager_Statics; \
public: \
	DECLARE_CLASS(ABuildManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ABuildManager)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABuildManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABuildManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuildManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuildManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuildManager(ABuildManager&&); \
	NO_API ABuildManager(const ABuildManager&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuildManager(ABuildManager&&); \
	NO_API ABuildManager(const ABuildManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuildManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuildManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABuildManager)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GhostTowerPrefab() { return STRUCT_OFFSET(ABuildManager, GhostTowerPrefab); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_9_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ABuildManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_BuildManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
