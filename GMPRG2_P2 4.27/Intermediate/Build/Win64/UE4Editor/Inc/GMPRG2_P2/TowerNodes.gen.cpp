// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/TowerNodes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerNodes() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerNodes_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerNodes();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
// End Cross Module References
	void ATowerNodes::StaticRegisterNativesATowerNodes()
	{
	}
	UClass* Z_Construct_UClass_ATowerNodes_NoRegister()
	{
		return ATowerNodes::StaticClass();
	}
	struct Z_Construct_UClass_ATowerNodes_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuildPos_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BuildPos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATowerNodes_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerNodes_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TowerNodes.h" },
		{ "ModuleRelativePath", "TowerNodes.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerNodes_Statics::NewProp_BuildPos_MetaData[] = {
		{ "Category", "TowerNodes" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TowerNodes.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATowerNodes_Statics::NewProp_BuildPos = { "BuildPos", nullptr, (EPropertyFlags)0x002008000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerNodes, BuildPos), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATowerNodes_Statics::NewProp_BuildPos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerNodes_Statics::NewProp_BuildPos_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATowerNodes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerNodes_Statics::NewProp_BuildPos,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATowerNodes_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATowerNodes>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATowerNodes_Statics::ClassParams = {
		&ATowerNodes::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ATowerNodes_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ATowerNodes_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATowerNodes_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerNodes_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATowerNodes()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATowerNodes_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATowerNodes, 3646317205);
	template<> GMPRG2_P2_API UClass* StaticClass<ATowerNodes>()
	{
		return ATowerNodes::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATowerNodes(Z_Construct_UClass_ATowerNodes, &ATowerNodes::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("ATowerNodes"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATowerNodes);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
