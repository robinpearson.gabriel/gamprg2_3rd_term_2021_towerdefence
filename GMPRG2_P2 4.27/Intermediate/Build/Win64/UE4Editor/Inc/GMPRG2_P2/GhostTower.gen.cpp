// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/GhostTower.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGhostTower() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_AGhostTower_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AGhostTower();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_AGhostTower_setRadius = FName(TEXT("setRadius"));
	void AGhostTower::setRadius(bool onNode)
	{
		GhostTower_eventsetRadius_Parms Parms;
		Parms.onNode=onNode ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AGhostTower_setRadius),&Parms);
	}
	void AGhostTower::StaticRegisterNativesAGhostTower()
	{
	}
	struct Z_Construct_UFunction_AGhostTower_setRadius_Statics
	{
		static void NewProp_onNode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_onNode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AGhostTower_setRadius_Statics::NewProp_onNode_SetBit(void* Obj)
	{
		((GhostTower_eventsetRadius_Parms*)Obj)->onNode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AGhostTower_setRadius_Statics::NewProp_onNode = { "onNode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GhostTower_eventsetRadius_Parms), &Z_Construct_UFunction_AGhostTower_setRadius_Statics::NewProp_onNode_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGhostTower_setRadius_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGhostTower_setRadius_Statics::NewProp_onNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGhostTower_setRadius_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GhostTower.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGhostTower_setRadius_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGhostTower, nullptr, "setRadius", nullptr, nullptr, sizeof(GhostTower_eventsetRadius_Parms), Z_Construct_UFunction_AGhostTower_setRadius_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGhostTower_setRadius_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGhostTower_setRadius_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGhostTower_setRadius_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGhostTower_setRadius()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGhostTower_setRadius_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AGhostTower_NoRegister()
	{
		return AGhostTower::StaticClass();
	}
	struct Z_Construct_UClass_AGhostTower_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Range_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Range;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_mesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGhostTower_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AGhostTower_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AGhostTower_setRadius, "setRadius" }, // 1540905710
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGhostTower_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GhostTower.h" },
		{ "ModuleRelativePath", "GhostTower.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGhostTower_Statics::NewProp_Range_MetaData[] = {
		{ "Category", "GhostTower" },
		{ "ModuleRelativePath", "GhostTower.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGhostTower_Statics::NewProp_Range = { "Range", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGhostTower, Range), METADATA_PARAMS(Z_Construct_UClass_AGhostTower_Statics::NewProp_Range_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGhostTower_Statics::NewProp_Range_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGhostTower_Statics::NewProp_mesh_MetaData[] = {
		{ "Category", "GhostTower" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "GhostTower.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGhostTower_Statics::NewProp_mesh = { "mesh", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGhostTower, mesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGhostTower_Statics::NewProp_mesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGhostTower_Statics::NewProp_mesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGhostTower_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGhostTower_Statics::NewProp_Range,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGhostTower_Statics::NewProp_mesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGhostTower_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGhostTower>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGhostTower_Statics::ClassParams = {
		&AGhostTower::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AGhostTower_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AGhostTower_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AGhostTower_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGhostTower_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGhostTower()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGhostTower_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGhostTower, 3502930250);
	template<> GMPRG2_P2_API UClass* StaticClass<AGhostTower>()
	{
		return AGhostTower::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGhostTower(Z_Construct_UClass_AGhostTower, &AGhostTower::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("AGhostTower"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGhostTower);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
