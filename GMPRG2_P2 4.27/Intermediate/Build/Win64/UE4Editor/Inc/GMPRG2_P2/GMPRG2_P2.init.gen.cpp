// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGMPRG2_P2_init() {}
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2_PlayerBaseHitSignaiture__DelegateSignature();
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2_PlayerDieSignaiture__DelegateSignature();
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2_PlayerKilledSignaiture__DelegateSignature();
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2_EnemyHitSignaiture__DelegateSignature();
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature();
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_GMPRG2_P2_PlayerBaseHitSignaiture__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_GMPRG2_P2_PlayerDieSignaiture__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_GMPRG2_P2_PlayerKilledSignaiture__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_GMPRG2_P2_EnemyHitSignaiture__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/GMPRG2_P2",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xC78A681E,
				0x92E0FE62,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
