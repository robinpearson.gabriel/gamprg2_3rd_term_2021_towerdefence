// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/PowerGeneratorBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePowerGeneratorBase() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_APowerGeneratorBase_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_APowerGeneratorBase();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	void APowerGeneratorBase::StaticRegisterNativesAPowerGeneratorBase()
	{
	}
	UClass* Z_Construct_UClass_APowerGeneratorBase_NoRegister()
	{
		return APowerGeneratorBase::StaticClass();
	}
	struct Z_Construct_UClass_APowerGeneratorBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bonus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Bonus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BonusIncrease;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RadiusIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadiusIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RadiusIncrease;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APowerGeneratorBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATowerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerGeneratorBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "PowerGeneratorBase.h" },
		{ "ModuleRelativePath", "PowerGeneratorBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_Bonus_MetaData[] = {
		{ "Category", "PowerGeneratorBase" },
		{ "ModuleRelativePath", "PowerGeneratorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_Bonus = { "Bonus", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APowerGeneratorBase, Bonus), METADATA_PARAMS(Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_Bonus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_Bonus_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_BonusIncrease_Inner = { "BonusIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_BonusIncrease_MetaData[] = {
		{ "Category", "PowerGeneratorBase" },
		{ "ModuleRelativePath", "PowerGeneratorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_BonusIncrease = { "BonusIncrease", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APowerGeneratorBase, BonusIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_BonusIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_BonusIncrease_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_RadiusIncrease_Inner = { "RadiusIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_RadiusIncrease_MetaData[] = {
		{ "Category", "PowerGeneratorBase" },
		{ "ModuleRelativePath", "PowerGeneratorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_RadiusIncrease = { "RadiusIncrease", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APowerGeneratorBase, RadiusIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_RadiusIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_RadiusIncrease_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APowerGeneratorBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_Bonus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_BonusIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_BonusIncrease,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_RadiusIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerGeneratorBase_Statics::NewProp_RadiusIncrease,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APowerGeneratorBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APowerGeneratorBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APowerGeneratorBase_Statics::ClassParams = {
		&APowerGeneratorBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APowerGeneratorBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_APowerGeneratorBase_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APowerGeneratorBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APowerGeneratorBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APowerGeneratorBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APowerGeneratorBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APowerGeneratorBase, 943616547);
	template<> GMPRG2_P2_API UClass* StaticClass<APowerGeneratorBase>()
	{
		return APowerGeneratorBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APowerGeneratorBase(Z_Construct_UClass_APowerGeneratorBase, &APowerGeneratorBase::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("APowerGeneratorBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APowerGeneratorBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
