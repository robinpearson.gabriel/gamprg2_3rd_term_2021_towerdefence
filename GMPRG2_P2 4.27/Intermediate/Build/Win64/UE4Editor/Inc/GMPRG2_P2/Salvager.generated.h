// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_Salvager_generated_h
#error "Salvager.generated.h already included, missing '#pragma once' in Salvager.h"
#endif
#define GMPRG2_P2_Salvager_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIncreaseDrop);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIncreaseDrop);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASalvager(); \
	friend struct Z_Construct_UClass_ASalvager_Statics; \
public: \
	DECLARE_CLASS(ASalvager, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ASalvager)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASalvager(); \
	friend struct Z_Construct_UClass_ASalvager_Statics; \
public: \
	DECLARE_CLASS(ASalvager, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ASalvager)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASalvager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASalvager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASalvager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASalvager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASalvager(ASalvager&&); \
	NO_API ASalvager(const ASalvager&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASalvager() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASalvager(ASalvager&&); \
	NO_API ASalvager(const ASalvager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASalvager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASalvager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASalvager)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__dropIncreaseRate() { return STRUCT_OFFSET(ASalvager, dropIncreaseRate); } \
	FORCEINLINE static uint32 __PPO__coinPerMine() { return STRUCT_OFFSET(ASalvager, coinPerMine); } \
	FORCEINLINE static uint32 __PPO__coinPerMineIncrease() { return STRUCT_OFFSET(ASalvager, coinPerMineIncrease); } \
	FORCEINLINE static uint32 __PPO__dropIncreaseIncrease() { return STRUCT_OFFSET(ASalvager, dropIncreaseIncrease); } \
	FORCEINLINE static uint32 __PPO__RangeIncrease() { return STRUCT_OFFSET(ASalvager, RangeIncrease); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_12_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ASalvager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_Salvager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
