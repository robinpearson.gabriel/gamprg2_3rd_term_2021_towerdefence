// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/EMP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEMP() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_AEMP_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AEMP();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	void AEMP::StaticRegisterNativesAEMP()
	{
	}
	UClass* Z_Construct_UClass_AEMP_NoRegister()
	{
		return AEMP::StaticClass();
	}
	struct Z_Construct_UClass_AEMP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEMP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATowerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEMP_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "EMP.h" },
		{ "ModuleRelativePath", "EMP.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEMP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEMP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEMP_Statics::ClassParams = {
		&AEMP::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AEMP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AEMP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEMP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEMP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEMP, 444623027);
	template<> GMPRG2_P2_API UClass* StaticClass<AEMP>()
	{
		return AEMP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEMP(Z_Construct_UClass_AEMP, &AEMP::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("AEMP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEMP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
