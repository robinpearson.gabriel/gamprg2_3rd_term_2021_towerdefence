// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/Salvager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSalvager() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_ASalvager_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ASalvager();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	DEFINE_FUNCTION(ASalvager::execIncreaseDrop)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Drop);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncreaseDrop(Z_Param_Drop);
		P_NATIVE_END;
	}
	void ASalvager::StaticRegisterNativesASalvager()
	{
		UClass* Class = ASalvager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IncreaseDrop", &ASalvager::execIncreaseDrop },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics
	{
		struct Salvager_eventIncreaseDrop_Parms
		{
			int32 Drop;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Drop;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::NewProp_Drop = { "Drop", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Salvager_eventIncreaseDrop_Parms, Drop), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::NewProp_Drop,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Salvager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASalvager, nullptr, "IncreaseDrop", nullptr, nullptr, sizeof(Salvager_eventIncreaseDrop_Parms), Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASalvager_IncreaseDrop()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASalvager_IncreaseDrop_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASalvager_NoRegister()
	{
		return ASalvager::StaticClass();
	}
	struct Z_Construct_UClass_ASalvager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_dropIncreaseRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_dropIncreaseRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_coinPerMine_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_coinPerMine;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_coinPerMineIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_coinPerMineIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_coinPerMineIncrease;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_dropIncreaseIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_dropIncreaseIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_dropIncreaseIncrease;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RangeIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RangeIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RangeIncrease;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASalvager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATowerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ASalvager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ASalvager_IncreaseDrop, "IncreaseDrop" }, // 2399820278
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASalvager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Salvager.h" },
		{ "ModuleRelativePath", "Salvager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseRate_MetaData[] = {
		{ "Category", "Salvager" },
		{ "ModuleRelativePath", "Salvager.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseRate = { "dropIncreaseRate", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASalvager, dropIncreaseRate), METADATA_PARAMS(Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMine_MetaData[] = {
		{ "Category", "Salvager" },
		{ "ModuleRelativePath", "Salvager.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMine = { "coinPerMine", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASalvager, coinPerMine), METADATA_PARAMS(Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMine_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMine_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMineIncrease_Inner = { "coinPerMineIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMineIncrease_MetaData[] = {
		{ "Category", "Salvager" },
		{ "ModuleRelativePath", "Salvager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMineIncrease = { "coinPerMineIncrease", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASalvager, coinPerMineIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMineIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMineIncrease_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseIncrease_Inner = { "dropIncreaseIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseIncrease_MetaData[] = {
		{ "Category", "Salvager" },
		{ "ModuleRelativePath", "Salvager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseIncrease = { "dropIncreaseIncrease", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASalvager, dropIncreaseIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseIncrease_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_RangeIncrease_Inner = { "RangeIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASalvager_Statics::NewProp_RangeIncrease_MetaData[] = {
		{ "Category", "Salvager" },
		{ "ModuleRelativePath", "Salvager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASalvager_Statics::NewProp_RangeIncrease = { "RangeIncrease", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASalvager, RangeIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASalvager_Statics::NewProp_RangeIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASalvager_Statics::NewProp_RangeIncrease_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASalvager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMineIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_coinPerMineIncrease,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_dropIncreaseIncrease,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_RangeIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASalvager_Statics::NewProp_RangeIncrease,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASalvager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASalvager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASalvager_Statics::ClassParams = {
		&ASalvager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ASalvager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ASalvager_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASalvager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASalvager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASalvager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASalvager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASalvager, 1050353157);
	template<> GMPRG2_P2_API UClass* StaticClass<ASalvager>()
	{
		return ASalvager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASalvager(Z_Construct_UClass_ASalvager, &ASalvager::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("ASalvager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASalvager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
