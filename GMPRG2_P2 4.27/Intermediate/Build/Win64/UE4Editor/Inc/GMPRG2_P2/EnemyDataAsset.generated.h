// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_EnemyDataAsset_generated_h
#error "EnemyDataAsset.generated.h already included, missing '#pragma once' in EnemyDataAsset.h"
#endif
#define GMPRG2_P2_EnemyDataAsset_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyDataAsset(); \
	friend struct Z_Construct_UClass_UEnemyDataAsset_Statics; \
public: \
	DECLARE_CLASS(UEnemyDataAsset, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UEnemyDataAsset)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyDataAsset(); \
	friend struct Z_Construct_UClass_UEnemyDataAsset_Statics; \
public: \
	DECLARE_CLASS(UEnemyDataAsset, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UEnemyDataAsset)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyDataAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyDataAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyDataAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyDataAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyDataAsset(UEnemyDataAsset&&); \
	NO_API UEnemyDataAsset(const UEnemyDataAsset&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyDataAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyDataAsset(UEnemyDataAsset&&); \
	NO_API UEnemyDataAsset(const UEnemyDataAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyDataAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyDataAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyDataAsset)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_PRIVATE_PROPERTY_OFFSET
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_14_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class UEnemyDataAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_EnemyDataAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
