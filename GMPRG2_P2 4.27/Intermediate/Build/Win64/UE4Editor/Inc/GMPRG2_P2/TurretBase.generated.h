// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_TurretBase_generated_h
#error "TurretBase.generated.h already included, missing '#pragma once' in TurretBase.h"
#endif
#define GMPRG2_P2_TurretBase_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATurretBase(); \
	friend struct Z_Construct_UClass_ATurretBase_Statics; \
public: \
	DECLARE_CLASS(ATurretBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATurretBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesATurretBase(); \
	friend struct Z_Construct_UClass_ATurretBase_Statics; \
public: \
	DECLARE_CLASS(ATurretBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATurretBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATurretBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATurretBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATurretBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATurretBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATurretBase(ATurretBase&&); \
	NO_API ATurretBase(const ATurretBase&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATurretBase(ATurretBase&&); \
	NO_API ATurretBase(const ATurretBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATurretBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATurretBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATurretBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Damage() { return STRUCT_OFFSET(ATurretBase, Damage); } \
	FORCEINLINE static uint32 __PPO__bulletType() { return STRUCT_OFFSET(ATurretBase, bulletType); } \
	FORCEINLINE static uint32 __PPO__fireRate() { return STRUCT_OFFSET(ATurretBase, fireRate); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_13_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ATurretBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_TurretBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
