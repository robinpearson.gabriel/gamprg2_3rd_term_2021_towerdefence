// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_Targets_generated_h
#error "Targets.generated.h already included, missing '#pragma once' in Targets.h"
#endif
#define GMPRG2_P2_Targets_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATargets(); \
	friend struct Z_Construct_UClass_ATargets_Statics; \
public: \
	DECLARE_CLASS(ATargets, AStaticMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATargets)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATargets(); \
	friend struct Z_Construct_UClass_ATargets_Statics; \
public: \
	DECLARE_CLASS(ATargets, AStaticMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATargets)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATargets(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATargets) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATargets); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATargets); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATargets(ATargets&&); \
	NO_API ATargets(const ATargets&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATargets(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATargets(ATargets&&); \
	NO_API ATargets(const ATargets&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATargets); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATargets); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATargets)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__targetOrderNum() { return STRUCT_OFFSET(ATargets, targetOrderNum); } \
	FORCEINLINE static uint32 __PPO__Route() { return STRUCT_OFFSET(ATargets, Route); } \
	FORCEINLINE static uint32 __PPO__NextTargetlist() { return STRUCT_OFFSET(ATargets, NextTargetlist); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_12_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ATargets>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_Targets_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
