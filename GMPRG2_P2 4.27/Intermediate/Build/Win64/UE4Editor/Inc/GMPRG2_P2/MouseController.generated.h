// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
struct FVector;
#ifdef GMPRG2_P2_MouseController_generated_h
#error "MouseController.generated.h already included, missing '#pragma once' in MouseController.h"
#endif
#define GMPRG2_P2_MouseController_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_13_DELEGATE \
static inline void F_RealeasedSigniture_DelegateWrapper(const FMulticastScriptDelegate& _RealeasedSigniture) \
{ \
	_RealeasedSigniture.ProcessMulticastDelegate<UObject>(NULL); \
}


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_12_DELEGATE \
struct _Script_GMPRG2_P2_event_PressedSigniture_Parms \
{ \
	AActor* HitObj; \
	FVector Pos; \
}; \
static inline void F_PressedSigniture_DelegateWrapper(const FMulticastScriptDelegate& _PressedSigniture, AActor* HitObj, FVector Pos) \
{ \
	_Script_GMPRG2_P2_event_PressedSigniture_Parms Parms; \
	Parms.HitObj=HitObj; \
	Parms.Pos=Pos; \
	_PressedSigniture.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execreleased); \
	DECLARE_FUNCTION(execclicked); \
	DECLARE_FUNCTION(execActivated);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execreleased); \
	DECLARE_FUNCTION(execclicked); \
	DECLARE_FUNCTION(execActivated);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMouseController(); \
	friend struct Z_Construct_UClass_AMouseController_Statics; \
public: \
	DECLARE_CLASS(AMouseController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AMouseController)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAMouseController(); \
	friend struct Z_Construct_UClass_AMouseController_Statics; \
public: \
	DECLARE_CLASS(AMouseController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AMouseController)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMouseController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMouseController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMouseController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMouseController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMouseController(AMouseController&&); \
	NO_API AMouseController(const AMouseController&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMouseController(AMouseController&&); \
	NO_API AMouseController(const AMouseController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMouseController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMouseController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMouseController)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_PRIVATE_PROPERTY_OFFSET
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_14_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AMouseController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_MouseController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
