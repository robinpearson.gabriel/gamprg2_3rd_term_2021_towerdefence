// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef GMPRG2_P2_LazerTower_generated_h
#error "LazerTower.generated.h already included, missing '#pragma once' in LazerTower.h"
#endif
#define GMPRG2_P2_LazerTower_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_EVENT_PARMS \
	struct LazerTower_eventDrawLine_Parms \
	{ \
		FVector Startpos; \
		FVector end; \
	};


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_CALLBACK_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALazerTower(); \
	friend struct Z_Construct_UClass_ALazerTower_Statics; \
public: \
	DECLARE_CLASS(ALazerTower, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ALazerTower)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_INCLASS \
private: \
	static void StaticRegisterNativesALazerTower(); \
	friend struct Z_Construct_UClass_ALazerTower_Statics; \
public: \
	DECLARE_CLASS(ALazerTower, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ALazerTower)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALazerTower(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALazerTower) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALazerTower); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALazerTower); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALazerTower(ALazerTower&&); \
	NO_API ALazerTower(const ALazerTower&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALazerTower() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALazerTower(ALazerTower&&); \
	NO_API ALazerTower(const ALazerTower&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALazerTower); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALazerTower); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALazerTower)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__damage() { return STRUCT_OFFSET(ALazerTower, damage); } \
	FORCEINLINE static uint32 __PPO__duration() { return STRUCT_OFFSET(ALazerTower, duration); } \
	FORCEINLINE static uint32 __PPO__fireRate() { return STRUCT_OFFSET(ALazerTower, fireRate); } \
	FORCEINLINE static uint32 __PPO__DamageIncrease() { return STRUCT_OFFSET(ALazerTower, DamageIncrease); } \
	FORCEINLINE static uint32 __PPO__fireRateIncrease() { return STRUCT_OFFSET(ALazerTower, fireRateIncrease); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_12_PROLOG \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_EVENT_PARMS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ALazerTower>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_LazerTower_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
