// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_HPComponent_generated_h
#error "HPComponent.generated.h already included, missing '#pragma once' in HPComponent.h"
#endif
#define GMPRG2_P2_HPComponent_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHPPercentage); \
	DECLARE_FUNCTION(execGetHP);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHPPercentage); \
	DECLARE_FUNCTION(execGetHP);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHPComponent(); \
	friend struct Z_Construct_UClass_UHPComponent_Statics; \
public: \
	DECLARE_CLASS(UHPComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UHPComponent)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUHPComponent(); \
	friend struct Z_Construct_UClass_UHPComponent_Statics; \
public: \
	DECLARE_CLASS(UHPComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UHPComponent)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHPComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHPComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHPComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHPComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHPComponent(UHPComponent&&); \
	NO_API UHPComponent(const UHPComponent&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHPComponent(UHPComponent&&); \
	NO_API UHPComponent(const UHPComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHPComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHPComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHPComponent)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_PRIVATE_PROPERTY_OFFSET
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_10_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class UHPComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_HPComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
