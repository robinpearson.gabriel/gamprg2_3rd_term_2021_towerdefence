// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/MissileBullet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMissileBullet() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_AMissileBullet_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AMissileBullet();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AProjectile();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	static FName NAME_AMissileBullet_hitsomething = FName(TEXT("hitsomething"));
	void AMissileBullet::hitsomething()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMissileBullet_hitsomething),NULL);
	}
	void AMissileBullet::StaticRegisterNativesAMissileBullet()
	{
	}
	struct Z_Construct_UFunction_AMissileBullet_hitsomething_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMissileBullet_hitsomething_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MissileBullet.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMissileBullet_hitsomething_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMissileBullet, nullptr, "hitsomething", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMissileBullet_hitsomething_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMissileBullet_hitsomething_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMissileBullet_hitsomething()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMissileBullet_hitsomething_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMissileBullet_NoRegister()
	{
		return AMissileBullet::StaticClass();
	}
	struct Z_Construct_UClass_AMissileBullet_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMissileBullet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AProjectile,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMissileBullet_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMissileBullet_hitsomething, "hitsomething" }, // 4057807300
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileBullet_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MissileBullet.h" },
		{ "ModuleRelativePath", "MissileBullet.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileBullet_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "MissileBullet" },
		{ "ModuleRelativePath", "MissileBullet.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMissileBullet_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileBullet, Radius), METADATA_PARAMS(Z_Construct_UClass_AMissileBullet_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileBullet_Statics::NewProp_Radius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMissileBullet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileBullet_Statics::NewProp_Radius,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMissileBullet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMissileBullet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMissileBullet_Statics::ClassParams = {
		&AMissileBullet::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMissileBullet_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMissileBullet_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMissileBullet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileBullet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMissileBullet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMissileBullet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMissileBullet, 2622651353);
	template<> GMPRG2_P2_API UClass* StaticClass<AMissileBullet>()
	{
		return AMissileBullet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMissileBullet(Z_Construct_UClass_AMissileBullet, &AMissileBullet::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("AMissileBullet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMissileBullet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
