// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/MouseController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMouseController() {}
// Cross Module References
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	GMPRG2_P2_API UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AMouseController_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AMouseController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GMPRG2_P2, nullptr, "_RealeasedSigniture__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics
	{
		struct _Script_GMPRG2_P2_event_PressedSigniture_Parms
		{
			AActor* HitObj;
			FVector Pos;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HitObj;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::NewProp_HitObj = { "HitObj", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GMPRG2_P2_event_PressedSigniture_Parms, HitObj), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::NewProp_Pos = { "Pos", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GMPRG2_P2_event_PressedSigniture_Parms, Pos), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::NewProp_HitObj,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::NewProp_Pos,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GMPRG2_P2, nullptr, "_PressedSigniture__DelegateSignature", nullptr, nullptr, sizeof(_Script_GMPRG2_P2_event_PressedSigniture_Parms), Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(AMouseController::execreleased)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->released();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMouseController::execclicked)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->clicked();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMouseController::execActivated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Activated();
		P_NATIVE_END;
	}
	void AMouseController::StaticRegisterNativesAMouseController()
	{
		UClass* Class = AMouseController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Activated", &AMouseController::execActivated },
			{ "clicked", &AMouseController::execclicked },
			{ "released", &AMouseController::execreleased },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMouseController_Activated_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMouseController_Activated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMouseController_Activated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMouseController, nullptr, "Activated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMouseController_Activated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMouseController_Activated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMouseController_Activated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMouseController_Activated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMouseController_clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMouseController_clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMouseController_clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMouseController, nullptr, "clicked", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMouseController_clicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMouseController_clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMouseController_clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMouseController_clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMouseController_released_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMouseController_released_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMouseController_released_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMouseController, nullptr, "released", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMouseController_released_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMouseController_released_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMouseController_released()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMouseController_released_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMouseController_NoRegister()
	{
		return AMouseController::StaticClass();
	}
	struct Z_Construct_UClass_AMouseController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_onMouseHit_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_onMouseHit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_onMouseReleased_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_onMouseReleased;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMouseController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMouseController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMouseController_Activated, "Activated" }, // 1957048132
		{ &Z_Construct_UFunction_AMouseController_clicked, "clicked" }, // 2590073077
		{ &Z_Construct_UFunction_AMouseController_released, "released" }, // 3037551860
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMouseController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MouseController.h" },
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseHit_MetaData[] = {
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseHit = { "onMouseHit", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMouseController, onMouseHit), Z_Construct_UDelegateFunction_GMPRG2_P2__PressedSigniture__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseHit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseHit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseReleased_MetaData[] = {
		{ "ModuleRelativePath", "MouseController.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseReleased = { "onMouseReleased", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMouseController, onMouseReleased), Z_Construct_UDelegateFunction_GMPRG2_P2__RealeasedSigniture__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseReleased_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseReleased_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMouseController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseHit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMouseController_Statics::NewProp_onMouseReleased,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMouseController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMouseController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMouseController_Statics::ClassParams = {
		&AMouseController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMouseController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMouseController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMouseController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMouseController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMouseController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMouseController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMouseController, 2479982771);
	template<> GMPRG2_P2_API UClass* StaticClass<AMouseController>()
	{
		return AMouseController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMouseController(Z_Construct_UClass_AMouseController, &AMouseController::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("AMouseController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMouseController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
