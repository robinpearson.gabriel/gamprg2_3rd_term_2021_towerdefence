// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AEnemyBase;
#ifdef GMPRG2_P2_GunComponent_generated_h
#error "GunComponent.generated.h already included, missing '#pragma once' in GunComponent.h"
#endif
#define GMPRG2_P2_GunComponent_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_RPC_WRAPPERS \
	virtual void Fire_Implementation(AEnemyBase* enemy); \
 \
	DECLARE_FUNCTION(execFire);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFire);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_EVENT_PARMS \
	struct GunComponent_eventFire_Parms \
	{ \
		AEnemyBase* enemy; \
	};


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_CALLBACK_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGunComponent(); \
	friend struct Z_Construct_UClass_UGunComponent_Statics; \
public: \
	DECLARE_CLASS(UGunComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UGunComponent)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUGunComponent(); \
	friend struct Z_Construct_UClass_UGunComponent_Statics; \
public: \
	DECLARE_CLASS(UGunComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UGunComponent)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGunComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGunComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGunComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGunComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGunComponent(UGunComponent&&); \
	NO_API UGunComponent(const UGunComponent&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGunComponent(UGunComponent&&); \
	NO_API UGunComponent(const UGunComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGunComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGunComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGunComponent)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Gun() { return STRUCT_OFFSET(UGunComponent, Gun); } \
	FORCEINLINE static uint32 __PPO__spawnpoint() { return STRUCT_OFFSET(UGunComponent, spawnpoint); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_10_PROLOG \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_EVENT_PARMS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class UGunComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_GunComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
