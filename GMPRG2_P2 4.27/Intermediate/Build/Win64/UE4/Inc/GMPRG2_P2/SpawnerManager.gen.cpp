// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/SpawnerManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpawnerManager() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_ASpawnerManager_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ASpawnerManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	void ASpawnerManager::StaticRegisterNativesASpawnerManager()
	{
	}
	UClass* Z_Construct_UClass_ASpawnerManager_NoRegister()
	{
		return ASpawnerManager::StaticClass();
	}
	struct Z_Construct_UClass_ASpawnerManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpawnerManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnerManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpawnerManager.h" },
		{ "ModuleRelativePath", "SpawnerManager.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpawnerManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpawnerManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpawnerManager_Statics::ClassParams = {
		&ASpawnerManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpawnerManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpawnerManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpawnerManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpawnerManager, 253111044);
	template<> GMPRG2_P2_API UClass* StaticClass<ASpawnerManager>()
	{
		return ASpawnerManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpawnerManager(Z_Construct_UClass_ASpawnerManager, &ASpawnerManager::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("ASpawnerManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpawnerManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
