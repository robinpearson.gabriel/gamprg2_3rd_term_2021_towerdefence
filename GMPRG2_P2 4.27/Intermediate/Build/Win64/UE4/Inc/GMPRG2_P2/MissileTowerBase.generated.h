// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_MissileTowerBase_generated_h
#error "MissileTowerBase.generated.h already included, missing '#pragma once' in MissileTowerBase.h"
#endif
#define GMPRG2_P2_MissileTowerBase_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMissileTowerBase(); \
	friend struct Z_Construct_UClass_AMissileTowerBase_Statics; \
public: \
	DECLARE_CLASS(AMissileTowerBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AMissileTowerBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMissileTowerBase(); \
	friend struct Z_Construct_UClass_AMissileTowerBase_Statics; \
public: \
	DECLARE_CLASS(AMissileTowerBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AMissileTowerBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMissileTowerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMissileTowerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMissileTowerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMissileTowerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMissileTowerBase(AMissileTowerBase&&); \
	NO_API AMissileTowerBase(const AMissileTowerBase&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMissileTowerBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMissileTowerBase(AMissileTowerBase&&); \
	NO_API AMissileTowerBase(const AMissileTowerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMissileTowerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMissileTowerBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMissileTowerBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Damage() { return STRUCT_OFFSET(AMissileTowerBase, Damage); } \
	FORCEINLINE static uint32 __PPO__bulletType() { return STRUCT_OFFSET(AMissileTowerBase, bulletType); } \
	FORCEINLINE static uint32 __PPO__fireRate() { return STRUCT_OFFSET(AMissileTowerBase, fireRate); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_12_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AMissileTowerBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileTowerBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
