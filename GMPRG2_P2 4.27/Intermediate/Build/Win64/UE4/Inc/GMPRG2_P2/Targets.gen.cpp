// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/Targets.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTargets() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATargets_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATargets();
	ENGINE_API UClass* Z_Construct_UClass_AStaticMeshActor();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void ATargets::StaticRegisterNativesATargets()
	{
	}
	UClass* Z_Construct_UClass_ATargets_NoRegister()
	{
		return ATargets::StaticClass();
	}
	struct Z_Construct_UClass_ATargets_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_targetOrderNum_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_targetOrderNum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Route_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Route;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NextTargetlist_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NextTargetlist_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NextTargetlist;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATargets_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AStaticMeshActor,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATargets_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Input" },
		{ "IncludePath", "Targets.h" },
		{ "ModuleRelativePath", "Targets.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATargets_Statics::NewProp_targetOrderNum_MetaData[] = {
		{ "Category", "Targets" },
		{ "ModuleRelativePath", "Targets.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ATargets_Statics::NewProp_targetOrderNum = { "targetOrderNum", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATargets, targetOrderNum), METADATA_PARAMS(Z_Construct_UClass_ATargets_Statics::NewProp_targetOrderNum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATargets_Statics::NewProp_targetOrderNum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATargets_Statics::NewProp_Route_MetaData[] = {
		{ "Category", "Targets" },
		{ "ModuleRelativePath", "Targets.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ATargets_Statics::NewProp_Route = { "Route", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATargets, Route), METADATA_PARAMS(Z_Construct_UClass_ATargets_Statics::NewProp_Route_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATargets_Statics::NewProp_Route_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATargets_Statics::NewProp_NextTargetlist_Inner = { "NextTargetlist", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATargets_Statics::NewProp_NextTargetlist_MetaData[] = {
		{ "Category", "Targets" },
		{ "ModuleRelativePath", "Targets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ATargets_Statics::NewProp_NextTargetlist = { "NextTargetlist", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATargets, NextTargetlist), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ATargets_Statics::NewProp_NextTargetlist_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATargets_Statics::NewProp_NextTargetlist_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATargets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATargets_Statics::NewProp_targetOrderNum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATargets_Statics::NewProp_Route,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATargets_Statics::NewProp_NextTargetlist_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATargets_Statics::NewProp_NextTargetlist,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATargets_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATargets>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATargets_Statics::ClassParams = {
		&ATargets::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ATargets_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ATargets_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATargets_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATargets_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATargets()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATargets_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATargets, 2199655379);
	template<> GMPRG2_P2_API UClass* StaticClass<ATargets>()
	{
		return ATargets::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATargets(Z_Construct_UClass_ATargets, &ATargets::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("ATargets"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATargets);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
