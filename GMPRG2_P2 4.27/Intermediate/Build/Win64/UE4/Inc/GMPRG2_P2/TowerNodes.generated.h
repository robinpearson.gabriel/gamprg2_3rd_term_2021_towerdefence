// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_TowerNodes_generated_h
#error "TowerNodes.generated.h already included, missing '#pragma once' in TowerNodes.h"
#endif
#define GMPRG2_P2_TowerNodes_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerNodes(); \
	friend struct Z_Construct_UClass_ATowerNodes_Statics; \
public: \
	DECLARE_CLASS(ATowerNodes, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATowerNodes)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATowerNodes(); \
	friend struct Z_Construct_UClass_ATowerNodes_Statics; \
public: \
	DECLARE_CLASS(ATowerNodes, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ATowerNodes)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerNodes(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerNodes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerNodes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerNodes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerNodes(ATowerNodes&&); \
	NO_API ATowerNodes(const ATowerNodes&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerNodes(ATowerNodes&&); \
	NO_API ATowerNodes(const ATowerNodes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerNodes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerNodes); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATowerNodes)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BuildPos() { return STRUCT_OFFSET(ATowerNodes, BuildPos); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_9_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ATowerNodes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_TowerNodes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
