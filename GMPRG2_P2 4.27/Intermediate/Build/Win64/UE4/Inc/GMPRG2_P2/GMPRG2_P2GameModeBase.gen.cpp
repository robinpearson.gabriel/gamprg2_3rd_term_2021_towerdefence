// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/GMPRG2_P2GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGMPRG2_P2GameModeBase() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_AGMPRG2_P2GameModeBase_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AGMPRG2_P2GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	void AGMPRG2_P2GameModeBase::StaticRegisterNativesAGMPRG2_P2GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AGMPRG2_P2GameModeBase_NoRegister()
	{
		return AGMPRG2_P2GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "GMPRG2_P2GameModeBase.h" },
		{ "ModuleRelativePath", "GMPRG2_P2GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGMPRG2_P2GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics::ClassParams = {
		&AGMPRG2_P2GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGMPRG2_P2GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGMPRG2_P2GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGMPRG2_P2GameModeBase, 4043663869);
	template<> GMPRG2_P2_API UClass* StaticClass<AGMPRG2_P2GameModeBase>()
	{
		return AGMPRG2_P2GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGMPRG2_P2GameModeBase(Z_Construct_UClass_AGMPRG2_P2GameModeBase, &AGMPRG2_P2GameModeBase::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("AGMPRG2_P2GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGMPRG2_P2GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
