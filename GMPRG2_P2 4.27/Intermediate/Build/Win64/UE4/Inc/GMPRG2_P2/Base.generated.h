// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GMPRG2_P2_Base_generated_h
#error "Base.generated.h already included, missing '#pragma once' in Base.h"
#endif
#define GMPRG2_P2_Base_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_8_DELEGATE \
static inline void FPlayerBaseHitSignaiture_DelegateWrapper(const FMulticastScriptDelegate& PlayerBaseHitSignaiture) \
{ \
	PlayerBaseHitSignaiture.ProcessMulticastDelegate<UObject>(NULL); \
}


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnActorOverlap);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnActorOverlap);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABase(); \
	friend struct Z_Construct_UClass_ABase_Statics; \
public: \
	DECLARE_CLASS(ABase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ABase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABase(); \
	friend struct Z_Construct_UClass_ABase_Statics; \
public: \
	DECLARE_CLASS(ABase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(ABase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABase(ABase&&); \
	NO_API ABase(const ABase&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABase(ABase&&); \
	NO_API ABase(const ABase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BoxTrigger() { return STRUCT_OFFSET(ABase, BoxTrigger); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_9_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class ABase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_Base_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
