// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/GunComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGunComponent() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_UGunComponent_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_UGunComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AEnemyBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UGunComponent::execFire)
	{
		P_GET_OBJECT(AEnemyBase,Z_Param_enemy);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Fire_Implementation(Z_Param_enemy);
		P_NATIVE_END;
	}
	static FName NAME_UGunComponent_Fire = FName(TEXT("Fire"));
	void UGunComponent::Fire(AEnemyBase* enemy)
	{
		GunComponent_eventFire_Parms Parms;
		Parms.enemy=enemy;
		ProcessEvent(FindFunctionChecked(NAME_UGunComponent_Fire),&Parms);
	}
	void UGunComponent::StaticRegisterNativesUGunComponent()
	{
		UClass* Class = UGunComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Fire", &UGunComponent::execFire },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGunComponent_Fire_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_enemy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGunComponent_Fire_Statics::NewProp_enemy = { "enemy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GunComponent_eventFire_Parms, enemy), Z_Construct_UClass_AEnemyBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGunComponent_Fire_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGunComponent_Fire_Statics::NewProp_enemy,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGunComponent_Fire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GunComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGunComponent_Fire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGunComponent, nullptr, "Fire", nullptr, nullptr, sizeof(GunComponent_eventFire_Parms), Z_Construct_UFunction_UGunComponent_Fire_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGunComponent_Fire_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGunComponent_Fire_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGunComponent_Fire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGunComponent_Fire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGunComponent_Fire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGunComponent_NoRegister()
	{
		return UGunComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGunComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Gun_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Gun;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_spawnpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_spawnpoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGunComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGunComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGunComponent_Fire, "Fire" }, // 3031490084
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGunComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "GunComponent.h" },
		{ "ModuleRelativePath", "GunComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGunComponent_Statics::NewProp_Gun_MetaData[] = {
		{ "Category", "GunComponent" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "GunComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGunComponent_Statics::NewProp_Gun = { "Gun", nullptr, (EPropertyFlags)0x002008000008001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGunComponent, Gun), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGunComponent_Statics::NewProp_Gun_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGunComponent_Statics::NewProp_Gun_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGunComponent_Statics::NewProp_spawnpoint_MetaData[] = {
		{ "Category", "GunComponent" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "GunComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGunComponent_Statics::NewProp_spawnpoint = { "spawnpoint", nullptr, (EPropertyFlags)0x002008000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGunComponent, spawnpoint), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGunComponent_Statics::NewProp_spawnpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGunComponent_Statics::NewProp_spawnpoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGunComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGunComponent_Statics::NewProp_Gun,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGunComponent_Statics::NewProp_spawnpoint,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGunComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGunComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGunComponent_Statics::ClassParams = {
		&UGunComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGunComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGunComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGunComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGunComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGunComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGunComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGunComponent, 3851162904);
	template<> GMPRG2_P2_API UClass* StaticClass<UGunComponent>()
	{
		return UGunComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGunComponent(Z_Construct_UClass_UGunComponent, &UGunComponent::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("UGunComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGunComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
