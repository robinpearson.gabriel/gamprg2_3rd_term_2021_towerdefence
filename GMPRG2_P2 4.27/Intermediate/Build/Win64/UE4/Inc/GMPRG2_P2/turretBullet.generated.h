// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_turretBullet_generated_h
#error "turretBullet.generated.h already included, missing '#pragma once' in turretBullet.h"
#endif
#define GMPRG2_P2_turretBullet_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAturretBullet(); \
	friend struct Z_Construct_UClass_AturretBullet_Statics; \
public: \
	DECLARE_CLASS(AturretBullet, AProjectile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AturretBullet)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAturretBullet(); \
	friend struct Z_Construct_UClass_AturretBullet_Statics; \
public: \
	DECLARE_CLASS(AturretBullet, AProjectile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AturretBullet)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AturretBullet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AturretBullet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AturretBullet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AturretBullet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AturretBullet(AturretBullet&&); \
	NO_API AturretBullet(const AturretBullet&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AturretBullet() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AturretBullet(AturretBullet&&); \
	NO_API AturretBullet(const AturretBullet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AturretBullet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AturretBullet); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AturretBullet)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_PRIVATE_PROPERTY_OFFSET
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_12_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AturretBullet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_turretBullet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
