// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/EMPBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEMPBase() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_AEMPBase_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AEMPBase();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	void AEMPBase::StaticRegisterNativesAEMPBase()
	{
	}
	UClass* Z_Construct_UClass_AEMPBase_NoRegister()
	{
		return AEMPBase::StaticClass();
	}
	struct Z_Construct_UClass_AEMPBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlowEffect_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SlowEffect;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_slowIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_slowIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_slowIncrease;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RangeIncreas_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RangeIncreas_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RangeIncreas;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEMPBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATowerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEMPBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "EMPBase.h" },
		{ "ModuleRelativePath", "EMPBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEMPBase_Statics::NewProp_SlowEffect_MetaData[] = {
		{ "Category", "EMPBase" },
		{ "ModuleRelativePath", "EMPBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AEMPBase_Statics::NewProp_SlowEffect = { "SlowEffect", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEMPBase, SlowEffect), METADATA_PARAMS(Z_Construct_UClass_AEMPBase_Statics::NewProp_SlowEffect_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEMPBase_Statics::NewProp_SlowEffect_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AEMPBase_Statics::NewProp_slowIncrease_Inner = { "slowIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEMPBase_Statics::NewProp_slowIncrease_MetaData[] = {
		{ "Category", "EMPBase" },
		{ "ModuleRelativePath", "EMPBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AEMPBase_Statics::NewProp_slowIncrease = { "slowIncrease", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEMPBase, slowIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AEMPBase_Statics::NewProp_slowIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEMPBase_Statics::NewProp_slowIncrease_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AEMPBase_Statics::NewProp_RangeIncreas_Inner = { "RangeIncreas", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEMPBase_Statics::NewProp_RangeIncreas_MetaData[] = {
		{ "Category", "EMPBase" },
		{ "ModuleRelativePath", "EMPBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AEMPBase_Statics::NewProp_RangeIncreas = { "RangeIncreas", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEMPBase, RangeIncreas), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AEMPBase_Statics::NewProp_RangeIncreas_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEMPBase_Statics::NewProp_RangeIncreas_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AEMPBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEMPBase_Statics::NewProp_SlowEffect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEMPBase_Statics::NewProp_slowIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEMPBase_Statics::NewProp_slowIncrease,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEMPBase_Statics::NewProp_RangeIncreas_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEMPBase_Statics::NewProp_RangeIncreas,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEMPBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEMPBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEMPBase_Statics::ClassParams = {
		&AEMPBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AEMPBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AEMPBase_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AEMPBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AEMPBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEMPBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEMPBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEMPBase, 3618945376);
	template<> GMPRG2_P2_API UClass* StaticClass<AEMPBase>()
	{
		return AEMPBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEMPBase(Z_Construct_UClass_AEMPBase, &AEMPBase::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("AEMPBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEMPBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
