// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_PowerGeneratorBase_generated_h
#error "PowerGeneratorBase.generated.h already included, missing '#pragma once' in PowerGeneratorBase.h"
#endif
#define GMPRG2_P2_PowerGeneratorBase_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPowerGeneratorBase(); \
	friend struct Z_Construct_UClass_APowerGeneratorBase_Statics; \
public: \
	DECLARE_CLASS(APowerGeneratorBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(APowerGeneratorBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPowerGeneratorBase(); \
	friend struct Z_Construct_UClass_APowerGeneratorBase_Statics; \
public: \
	DECLARE_CLASS(APowerGeneratorBase, ATowerBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(APowerGeneratorBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APowerGeneratorBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APowerGeneratorBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerGeneratorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerGeneratorBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerGeneratorBase(APowerGeneratorBase&&); \
	NO_API APowerGeneratorBase(const APowerGeneratorBase&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APowerGeneratorBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerGeneratorBase(APowerGeneratorBase&&); \
	NO_API APowerGeneratorBase(const APowerGeneratorBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerGeneratorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerGeneratorBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APowerGeneratorBase)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Bonus() { return STRUCT_OFFSET(APowerGeneratorBase, Bonus); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_12_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class APowerGeneratorBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_PowerGeneratorBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
