// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/MissileTowerBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMissileTowerBase() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_AMissileTowerBase_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AMissileTowerBase();
	GMPRG2_P2_API UClass* Z_Construct_UClass_ATowerBase();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GMPRG2_P2_API UClass* Z_Construct_UClass_AProjectile_NoRegister();
// End Cross Module References
	void AMissileTowerBase::StaticRegisterNativesAMissileTowerBase()
	{
	}
	UClass* Z_Construct_UClass_AMissileTowerBase_NoRegister()
	{
		return AMissileTowerBase::StaticClass();
	}
	struct Z_Construct_UClass_AMissileTowerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bulletType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_bulletType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fireRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fireRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExplotionRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ExplotionRadius;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_explosionRadiusIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_explosionRadiusIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_explosionRadiusIncrease;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DamageIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DamageIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DamageIncrease;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fireRateIncrease_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fireRateIncrease_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_fireRateIncrease;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMissileTowerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATowerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MissileTowerBase.h" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "MissileTowerBase" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_Damage = { "Damage", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileTowerBase, Damage), METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_Damage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_Damage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_bulletType_MetaData[] = {
		{ "Category", "MissileTowerBase" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_bulletType = { "bulletType", nullptr, (EPropertyFlags)0x0024080000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileTowerBase, bulletType), Z_Construct_UClass_AProjectile_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_bulletType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_bulletType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRate_MetaData[] = {
		{ "Category", "MissileTowerBase" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRate = { "fireRate", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileTowerBase, fireRate), METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_ExplotionRadius_MetaData[] = {
		{ "Category", "MissileTowerBase" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_ExplotionRadius = { "ExplotionRadius", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileTowerBase, ExplotionRadius), METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_ExplotionRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_ExplotionRadius_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_explosionRadiusIncrease_Inner = { "explosionRadiusIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_explosionRadiusIncrease_MetaData[] = {
		{ "Category", "MissileTowerBase" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_explosionRadiusIncrease = { "explosionRadiusIncrease", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileTowerBase, explosionRadiusIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_explosionRadiusIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_explosionRadiusIncrease_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_DamageIncrease_Inner = { "DamageIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_DamageIncrease_MetaData[] = {
		{ "Category", "MissileTowerBase" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_DamageIncrease = { "DamageIncrease", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileTowerBase, DamageIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_DamageIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_DamageIncrease_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRateIncrease_Inner = { "fireRateIncrease", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRateIncrease_MetaData[] = {
		{ "Category", "MissileTowerBase" },
		{ "ModuleRelativePath", "MissileTowerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRateIncrease = { "fireRateIncrease", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMissileTowerBase, fireRateIncrease), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRateIncrease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRateIncrease_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMissileTowerBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_bulletType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_ExplotionRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_explosionRadiusIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_explosionRadiusIncrease,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_DamageIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_DamageIncrease,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRateIncrease_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMissileTowerBase_Statics::NewProp_fireRateIncrease,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMissileTowerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMissileTowerBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMissileTowerBase_Statics::ClassParams = {
		&AMissileTowerBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMissileTowerBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMissileTowerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMissileTowerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMissileTowerBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMissileTowerBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMissileTowerBase, 657607339);
	template<> GMPRG2_P2_API UClass* StaticClass<AMissileTowerBase>()
	{
		return AMissileTowerBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMissileTowerBase(Z_Construct_UClass_AMissileTowerBase, &AMissileTowerBase::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("AMissileTowerBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMissileTowerBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
