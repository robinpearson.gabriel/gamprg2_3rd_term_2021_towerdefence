// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_MissileBullet_generated_h
#error "MissileBullet.generated.h already included, missing '#pragma once' in MissileBullet.h"
#endif
#define GMPRG2_P2_MissileBullet_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_RPC_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_EVENT_PARMS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_CALLBACK_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMissileBullet(); \
	friend struct Z_Construct_UClass_AMissileBullet_Statics; \
public: \
	DECLARE_CLASS(AMissileBullet, AProjectile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AMissileBullet)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMissileBullet(); \
	friend struct Z_Construct_UClass_AMissileBullet_Statics; \
public: \
	DECLARE_CLASS(AMissileBullet, AProjectile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AMissileBullet)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMissileBullet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMissileBullet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMissileBullet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMissileBullet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMissileBullet(AMissileBullet&&); \
	NO_API AMissileBullet(const AMissileBullet&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMissileBullet() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMissileBullet(AMissileBullet&&); \
	NO_API AMissileBullet(const AMissileBullet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMissileBullet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMissileBullet); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMissileBullet)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_PRIVATE_PROPERTY_OFFSET
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_12_PROLOG \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_EVENT_PARMS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AMissileBullet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_MissileBullet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
