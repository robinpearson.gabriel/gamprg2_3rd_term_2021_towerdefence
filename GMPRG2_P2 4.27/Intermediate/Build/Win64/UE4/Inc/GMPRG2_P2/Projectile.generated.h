// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FHitResult;
#ifdef GMPRG2_P2_Projectile_generated_h
#error "Projectile.generated.h already included, missing '#pragma once' in Projectile.h"
#endif
#define GMPRG2_P2_Projectile_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_10_DELEGATE \
static inline void FEnemyHitSignaiture_DelegateWrapper(const FMulticastScriptDelegate& EnemyHitSignaiture) \
{ \
	EnemyHitSignaiture.ProcessMulticastDelegate<UObject>(NULL); \
}


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_RPC_WRAPPERS \
	virtual void OnHit_Implementation(AActor* enemy); \
 \
	DECLARE_FUNCTION(execOnHit); \
	DECLARE_FUNCTION(execOnEnemyOverlap);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit); \
	DECLARE_FUNCTION(execOnEnemyOverlap);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_EVENT_PARMS \
	struct Projectile_eventOnHit_Parms \
	{ \
		AActor* enemy; \
	};


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_CALLBACK_WRAPPERS
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectile(); \
	friend struct Z_Construct_UClass_AProjectile_Statics; \
public: \
	DECLARE_CLASS(AProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AProjectile)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAProjectile(); \
	friend struct Z_Construct_UClass_AProjectile_Statics; \
public: \
	DECLARE_CLASS(AProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(AProjectile)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectile(AProjectile&&); \
	NO_API AProjectile(const AProjectile&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectile(AProjectile&&); \
	NO_API AProjectile(const AProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectile)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Damage() { return STRUCT_OFFSET(AProjectile, Damage); }


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_11_PROLOG \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_EVENT_PARMS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_CALLBACK_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class AProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
