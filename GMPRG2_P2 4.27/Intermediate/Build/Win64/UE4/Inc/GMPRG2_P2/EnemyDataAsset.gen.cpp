// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GMPRG2_P2/EnemyDataAsset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnemyDataAsset() {}
// Cross Module References
	GMPRG2_P2_API UClass* Z_Construct_UClass_UEnemyDataAsset_NoRegister();
	GMPRG2_P2_API UClass* Z_Construct_UClass_UEnemyDataAsset();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	UPackage* Z_Construct_UPackage__Script_GMPRG2_P2();
// End Cross Module References
	void UEnemyDataAsset::StaticRegisterNativesUEnemyDataAsset()
	{
	}
	UClass* Z_Construct_UClass_UEnemyDataAsset_NoRegister()
	{
		return UEnemyDataAsset::StaticClass();
	}
	struct Z_Construct_UClass_UEnemyDataAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnemyDataAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_GMPRG2_P2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyDataAsset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "EnemyDataAsset.h" },
		{ "ModuleRelativePath", "EnemyDataAsset.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnemyDataAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnemyDataAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnemyDataAsset_Statics::ClassParams = {
		&UEnemyDataAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEnemyDataAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyDataAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnemyDataAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnemyDataAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnemyDataAsset, 2747468113);
	template<> GMPRG2_P2_API UClass* StaticClass<UEnemyDataAsset>()
	{
		return UEnemyDataAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnemyDataAsset(Z_Construct_UClass_UEnemyDataAsset, &UEnemyDataAsset::StaticClass, TEXT("/Script/GMPRG2_P2"), TEXT("UEnemyDataAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnemyDataAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
