// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_P2_WaveData_generated_h
#error "WaveData.generated.h already included, missing '#pragma once' in WaveData.h"
#endif
#define GMPRG2_P2_WaveData_generated_h

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FenemyToSpawn_Statics; \
	GMPRG2_P2_API static class UScriptStruct* StaticStruct();


template<> GMPRG2_P2_API UScriptStruct* StaticStruct<struct FenemyToSpawn>();

#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_SPARSE_DATA
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDuration);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDuration);


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWaveData(); \
	friend struct Z_Construct_UClass_UWaveData_Statics; \
public: \
	DECLARE_CLASS(UWaveData, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UWaveData)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_INCLASS \
private: \
	static void StaticRegisterNativesUWaveData(); \
	friend struct Z_Construct_UClass_UWaveData_Statics; \
public: \
	DECLARE_CLASS(UWaveData, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GMPRG2_P2"), NO_API) \
	DECLARE_SERIALIZER(UWaveData)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWaveData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWaveData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWaveData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWaveData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWaveData(UWaveData&&); \
	NO_API UWaveData(const UWaveData&); \
public:


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWaveData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWaveData(UWaveData&&); \
	NO_API UWaveData(const UWaveData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWaveData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWaveData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWaveData)


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_PRIVATE_PROPERTY_OFFSET
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_31_PROLOG
#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_RPC_WRAPPERS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_INCLASS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_PRIVATE_PROPERTY_OFFSET \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_SPARSE_DATA \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_INCLASS_NO_PURE_DECLS \
	GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h_34_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_P2_API UClass* StaticClass<class UWaveData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GMPRG2_P2_4_27_Source_GMPRG2_P2_WaveData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
