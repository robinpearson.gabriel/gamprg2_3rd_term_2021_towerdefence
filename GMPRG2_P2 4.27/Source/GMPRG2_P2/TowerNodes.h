// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerNodes.generated.h"

UCLASS()
class GMPRG2_P2_API ATowerNodes : public AActor
{
	GENERATED_BODY()

public:

	// Sets default values for this actor's properties
	ATowerNodes();
	bool GetIsOccupied();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* BuildPos;
	bool IsOcuppied;



	class ATowerBase* towerBuilt;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void buildTower(TSubclassOf<class ATowerBase> TowerClass,int32 value);
	void RemoveTower();
};
