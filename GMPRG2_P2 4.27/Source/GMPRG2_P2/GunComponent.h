// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GunComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GMPRG2_P2_API UGunComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGunComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	class UStaticMeshComponent* Gun;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UArrowComponent* spawnpoint;

	
public:
	UFUNCTION(BlueprintNativeEvent)
	void Fire(AEnemyBase* enemy);

	void Fire_Implementation(AEnemyBase* enemy);

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
