// Fill out your copyright notice in the Description page of Project Settings.
#include "TurretBase.h"
#include "Projectile.h"
#include "Engine/EngineTypes.h"
#include "EnemyBase.h"
#include "GameFramework/ProjectileMovementComponent.h"




void ATurretBase::getEnemy(AActor* enemy)
{
	Super::getEnemy(enemy);

	if (AEnemyBase* targ = Cast<AEnemyBase>(enemy))

	{
		targets.AddUnique(enemy);
		//if (!target->IsValidLowLevelFast())
		//{
		//	target=targets[0];
		//}
	}
}

ATurretBase::ATurretBase()
{
	basefireRate = fireRate;
}

void ATurretBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!target->IsValidLowLevel())
	{
		RemoveFromList(target);
		if (targets.Num() > 0)
		{
			target = targets[0];
		
		}
	
	}
	else
	{
	
		Fire(target);
		Shoot();
	//GetWorldTimerManager().SetTimer(TimerHanler,this, &ATurretBase::Shoot, 1.0f, true);
	}


	
}
void ATurretBase::Overloaded(float Bonus)
{
	Super::Overloaded(Bonus);
	fireRate *= Bonus;
}
void ATurretBase::UndoOverload()
{
	Super::UndoOverload();
	fireRate = basefireRate;
}
void ATurretBase::SetInformation()
{
	Super::SetInformation();
	Name = "Turret";
	Stats = "Radius :" + FString::FromInt(Radius) + "\n" + "Fire Rate :" + FString::SanitizeFloat(fireRate) + "\n" + "Damage :" + FString::SanitizeFloat(Damage);
}
void ATurretBase::Upgrade()
{

	Super::Upgrade();
	fireRate = fireRateIncrease[level-1];
	Damage = DamageIncrease[level - 1];
	Gun->SetStaticMesh(meshes[level - 1]);
	
}
void ATurretBase::Shoot()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.bNoFail = true;
	SpawnParams.Owner = this;
	//SpawnParams.Instigator = this;

	FTransform pos;


	pos = GetPivotTransform();
	if (AProjectile* bullet = Cast<AProjectile>(GetWorld()->SpawnActor<AProjectile>(bulletType, pos, SpawnParams)))
	{
		bullet->SetDamage(Damage);

	}

	
		
}