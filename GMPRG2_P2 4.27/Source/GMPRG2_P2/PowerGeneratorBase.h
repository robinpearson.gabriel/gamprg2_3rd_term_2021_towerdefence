// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "PowerGeneratorBase.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API APowerGeneratorBase : public ATowerBase
{
	GENERATED_BODY()
		virtual void targetExit(AActor* target)override;
	virtual void getEnemy(AActor* element)override;
	UPROPERTY(EditAnywhere)
		float Bonus;
protected:
	void CheckAgain();
	virtual void BeginPlay() override;
public:
	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	TArray<float>BonusIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<float> RadiusIncrease;
	virtual void SetInformation()override;
	virtual void Upgrade()override;
};
