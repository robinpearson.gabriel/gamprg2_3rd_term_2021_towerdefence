// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Math/UnrealMathUtility.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenceGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API ATowerDefenceGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
		UFUNCTION(BlueprintPure, BlueprintCallable)
		int32 GetHP();
	int32 totalWaveEnemy;
	int32 EnemyLeft;
	int CurrentwaveIndex;

protected:


	
	TArray<AActor*>Bases;

	int32 Money;



	UPROPERTY(EditAnywhere)
		TArray < class UWaveData* > wavedatas;
	
	TArray< AActor*>Spawners;
	int32 HP;
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable)
	void WaveStart( class UWaveData* wavedata);


	void GetSpawners();

	UFUNCTION(BlueprintCallable)
	AActor* GetEnemySpawner(int index);

	//void SpawnEnemy();

	//void SetSpawnPoints();
	

	UFUNCTION()
	void TakeDamage();
	
	void nextwave();
public:
	
	void BindtoEnemy(class AEnemyBase* enemy);
	UFUNCTION(BlueprintCallable,BlueprintPure)
	float GetwaveProgression();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 gettoTalWaves();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 getCurrentWaveIndex();
	UFUNCTION()
	void enemyDied();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetMoney();

	UFUNCTION()
	void IncreaseMoney(int32 increase);
	UFUNCTION(BlueprintCallable)
	void Buy(int32 Decrease);


	UFUNCTION(BlueprintImplementableEvent)
	void Win();
	UFUNCTION(BlueprintImplementableEvent)
		void Lose();
};


