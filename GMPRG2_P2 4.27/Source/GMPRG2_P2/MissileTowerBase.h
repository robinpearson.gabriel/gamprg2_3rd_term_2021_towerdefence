// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "MissileTowerBase.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API AMissileTowerBase : public ATowerBase
{
	GENERATED_BODY()
protected:

	UPROPERTY(EditAnywhere, BluePrintReadOnly)
		float Damage;
	void Shoot();
	UPROPERTY(EditAnywhere, BluePrintReadOnly)
		TSubclassOf<class AProjectile> bulletType;
	float BasefireRate;
	UPROPERTY(EditAnywhere, BluePrintReadOnly)
		float fireRate;


	virtual void getEnemy(AActor* enemy) override;
public:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)

	float ExplotionRadius;
	virtual void Tick(float DeltaTime) override;
	virtual void Overloaded(float Bonus)override;
	virtual void UndoOverload()override;
	virtual void SetInformation()override;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	TArray<float> explosionRadiusIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<float> DamageIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<float> fireRateIncrease;
	virtual void Upgrade()override;
};
