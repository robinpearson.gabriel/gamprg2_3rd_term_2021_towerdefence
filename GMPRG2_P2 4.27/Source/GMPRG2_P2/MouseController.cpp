// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerNodes.h"
#include "DrawDebugHelpers.h"
#include "MouseController.h"

void AMouseController::Activated()
{
	
}

AMouseController::AMouseController()
{
	bShowMouseCursor = true;
	bEnableMouseOverEvents = true;
	//DefaultMouseCursor = EMouseCursor::Default;
}

void AMouseController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FHitResult hit;
	this->GetHitResultUnderCursor(ECollisionChannel::ECC_WorldStatic, false, hit);
	if (hit.bBlockingHit)
	{

		onMouseHit.Broadcast(hit.GetActor(), hit.ImpactPoint);
		
	}


	//mouse();
}



void AMouseController::BeginPlay()
{

	Super::BeginPlay();
}
void AMouseController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("LeftMouse",IE_Pressed,this,&AMouseController::clicked);
	InputComponent->BindAction("LeftMouse", IE_Released, this, &AMouseController::released);
}

void AMouseController::mouse()
{

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("World delta for current frame equals %f"), GetWorld()->TimeSeconds));
}


void AMouseController::clicked()
{
	onMouseReleased.Broadcast();
}

void AMouseController::released()
{
	//
}

