// Fill out your copyright notice in the Description page of Project Settings.
#include "TowerData.h"
#include "TowerBase.h"
#include "GhostTower.h"
#include "TowerDefenceGameMode.h"
#include "MouseController.h"
#include "TowerNodes.h"
#include "BuildManager.h"

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	canBuild = false;
}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	AMouseController* mouse = Cast<AMouseController>(GetWorld()->GetFirstPlayerController());
	mouse->onMouseHit.AddDynamic(this, &ABuildManager::checkTowers);
	mouse->onMouseReleased.AddDynamic(this, &ABuildManager::OpenTowerHUD);

}

void ABuildManager::SpawnGhost(UTowerData* data)
{
	
	

	if (Ghost != nullptr)
	{
		Ghost->Destroy();
	}
	TowerData = data;
	
	//SpawnParams.Instigator = this;
	if (AMouseController* mouse = Cast<AMouseController>(GetWorld()->GetFirstPlayerController()))
	{
		FTransform pos;
		pos = GetActorTransform();
		Ghost = Cast<AGhostTower>(GetWorld()->SpawnActor<AGhostTower>(GhostTowerPrefab, pos));
		Ghost->SetupGhost(data);
	//	AMouseController* mouse = Cast<AMouseController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
		mouse->onMouseHit.AddDynamic(this, &ABuildManager::CheckNode);
		mouse->onMouseReleased.AddDynamic(this, &ABuildManager::EndChecking);

		
	if (OpenTower->IsValidLowLevel())
	{
		OpenTower->OPenCloseHUD(false);
		OpenTower = nullptr;
	}
	}


}
void ABuildManager::CheckNode(AActor* actor, FVector pos)
{GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("World delta for current frame equals %f"), GetWorld()->TimeSeconds));
	if (ATowerNodes* dnode = Cast<ATowerNodes>(actor))
	{
		if (!dnode->GetIsOccupied())
		{
			canBuild = true;
			node = dnode;
		}

	}
	else
	{
		canBuild = false;
	}
	Ghost->SetPos(pos, canBuild);
}

void ABuildManager::EndChecking()
{
	AMouseController* mouse = Cast<AMouseController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
	mouse->onMouseHit.RemoveDynamic(this, &ABuildManager::CheckNode);
	mouse->onMouseReleased.RemoveDynamic(this, &ABuildManager::EndChecking);
	if (canBuild)
	{
		node->buildTower(TowerData->TowerClass, TowerData->Cost);
		if (ATowerDefenceGameMode* GameMode = Cast<ATowerDefenceGameMode>(GetWorld()->GetAuthGameMode()))
		{
			GameMode->Buy(TowerData->Cost);
		}

		canBuild = false;
	}


	Ghost->Destroy();
}

void ABuildManager::OpenTowerHUD()
{
	if (canOpen)
	{
		if (OpenTower == HitTower)
		{//clicked the same tower
			OpenTower->OPenCloseHUD(false);
			OpenTower = nullptr;
		}
		else if (OpenTower != nullptr)
		{
			//clicked a diff tower while an other tower is open
			OpenTower->OPenCloseHUD(false);
			OpenTower = HitTower;
			OpenTower->OPenCloseHUD(true);
		}
		else
		{
		//clicked tower nothing Open
			OpenTower = HitTower;
			OpenTower->OPenCloseHUD(true);
		}
	}
	else
	{
		//clicked nothing
		if (OpenTower->IsValidLowLevel())
		{
			OpenTower->OPenCloseHUD(false);
			OpenTower = nullptr;
		}
	}

}

void ABuildManager::checkTowers(AActor* actor, FVector pos)
{
	if (ATowerBase* tower = Cast<ATowerBase>(actor))
	{
		HitTower = tower;
		canOpen = true;

	}
	else
	{
		canOpen = false;
		HitTower = nullptr;
	}
}



// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

