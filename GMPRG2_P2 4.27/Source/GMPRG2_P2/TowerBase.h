// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GunComponent.h"
#include "TowerBase.generated.h"

UCLASS()
class GMPRG2_P2_API ATowerBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATowerBase();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(BlueprintReadOnly)
		class USceneComponent* root;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 Radius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* collider;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* Gun;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* GunBase;
	FTransform GetPivotTransform();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> targets;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		FString Name;
	UPROPERTY(BlueprintReadWrite)
		FString Stats;
	UPROPERTY(BlueprintReadOnly)
		int SellCost;
	



	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float level;//level and Cost
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int32> Cost;
	UFUNCTION(BlueprintImplementableEvent)
		void getTargets(AActor* element);

	

	UPROPERTY(EditAnywhere)
		TArray< UStaticMesh*> meshes;

	virtual void getEnemy(AActor* element);

	virtual void targetExit(AActor* element);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* target;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* pivot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* spawnpoint;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void Fire(AActor* enemy);

	void Fire_Implementation(AActor* enemy);
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
		//class UGunComponent* gun;
	bool isOverloaded;
	UFUNCTION()
		void OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnActorExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Overloaded(float bonus);
	virtual void UndoOverload();

	bool GetOverloaded();
	void IncreaseSellValue(int32 value);
	void RemoveFromList(AActor* obj);
UFUNCTION(BlueprintCallable)
		void SellTower();
	UFUNCTION(BlueprintImplementableEvent)
		void OPenCloseHUD(bool Open);

	UFUNCTION(BlueprintCallable)
		virtual void SetInformation();

	UFUNCTION(BlueprintCallable)
	virtual int32 getUpgradeCost();

	class ATowerNodes* node;
UFUNCTION(BlueprintCallable)
		virtual void Upgrade();
UFUNCTION(BlueprintCallable)
 bool GetCanUpgrade(int32 money);

	void SetNode(ATowerNodes* Node);
};
