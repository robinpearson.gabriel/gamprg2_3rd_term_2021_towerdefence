// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EnemyBase.h"
#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "EnemyDataAsset.generated.h"


/**
 * 
 */
UCLASS(BlueprintType)
class GMPRG2_P2_API UEnemyDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
	//	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//TSubclassOf < AEnemyBase> enemyClass;


	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//	int32 count;

};
