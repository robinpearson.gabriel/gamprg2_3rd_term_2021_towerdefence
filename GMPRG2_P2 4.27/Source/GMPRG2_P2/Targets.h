// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Targets.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API ATargets : public AStaticMeshActor
{
	GENERATED_BODY()
	protected:
	UPROPERTY(EditAnywhere)
	int32 targetOrderNum;
	UPROPERTY(EditAnywhere)
	int32 Route;

	

	UPROPERTY(EditAnywhere)
	TArray<AActor*>NextTargetlist;
public:
	
	int32 GettargetOrderNum();

	
	int32 GettargetRoute();


	AActor* GetNextTarget(int32 route);
};
