// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "EMPBase.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API AEMPBase : public ATowerBase
{
	GENERATED_BODY()
		virtual void targetExit(AActor* target)override;
	virtual void getEnemy(AActor* element)override;

protected:
	UPROPERTY(EditAnywhere)
		float SlowEffect;
	UPROPERTY(EditAnywhere)
	TArray<float> slowIncrease;
	UPROPERTY(EditAnywhere)
	TArray<float> RangeIncreas;
protected:
	virtual void BeginPlay() override;
	
	void CheckAgain();

	virtual void SetInformation()override;
public:
	virtual void Upgrade()override;
};
