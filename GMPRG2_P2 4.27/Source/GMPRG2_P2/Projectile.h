// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnemyHitSignaiture);
UCLASS()
class GMPRG2_P2_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	

	// Sets default values for this actor's properties
	AProjectile();
	void SetTarget(AActor* enemy);
	UPROPERTY(BlueprintAssignable)
	FEnemyHitSignaiture onHit;
	AActor* target;
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
class UProjectileMovementComponent* physics;
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
class	USphereComponent* body;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly)
	float Damage;

	UFUNCTION()
		void OnEnemyOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	float LifeSpan;

	void SetDamage(float Dmg);


	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnHit(AActor* enemy);
	void OnHit_Implementation(AActor* enemy);
	virtual void hit(AActor* enemy);
	float GetDamage();
	virtual void setexplosionRadius(float radius);
};
