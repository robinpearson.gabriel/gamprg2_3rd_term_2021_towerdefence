// Fill out your copyright notice in the Description page of Project Settings.


#include "LazerTower.h"
#include "EnemyBase.h"
#include "Components/ArrowComponent.h"
void ALazerTower::targetExit(AActor* actor)
{
	Super::targetExit(actor);
	if (AEnemyBase* enemy = Cast<AEnemyBase>(actor))
	{
	

	}
}

void ALazerTower::getEnemy(AActor* element)
{
	Super::getEnemy(element);
	if (AEnemyBase* enemy = Cast<AEnemyBase>(element))
	{
		targets.AddUnique(enemy);

	}
}

void ALazerTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!target->IsValidLowLevel())
	{
		RemoveFromList(target);
		if (targets.Num() > 0)
		{
			target = targets[0];

		}

	}
	else
	{

		Fire(target);
		HitTarget();
		
	}
}

void ALazerTower::HitTarget()
{
	
	DrawLine(pivot->GetComponentLocation(),target->GetActorLocation());
	if (AEnemyBase * enem = Cast<AEnemyBase>(target))
	{
		
			enem->OnBurn(damage,true,duration, fireRate);
	}
}

void ALazerTower::SetInformation()
{
	Super::SetInformation();
	Name = "Missile Tower";
	Stats = "Radius :" + FString::FromInt(Radius) + "\n" + "Fire Rate :" + FString::SanitizeFloat(duration) + "\n" + "Damage :" + FString::SanitizeFloat(damage);
}

void ALazerTower::Upgrade()
{
	Super::Upgrade();
	fireRate = fireRateIncrease[level - 1];
	damage = DamageIncrease[level - 1];

	Gun->SetStaticMesh(meshes[level - 1]);
}
