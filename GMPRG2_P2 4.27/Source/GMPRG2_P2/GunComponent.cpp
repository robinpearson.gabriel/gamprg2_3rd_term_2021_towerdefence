// Fill out your copyright notice in the Description page of Project Settings.
#include "GunComponent.h"
#include "EnemyBase.h"

#include "Engine/StaticMesh.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"


// Sets default values for this component's properties
UGunComponent::UGunComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	Gun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("gun"));

	spawnpoint = CreateDefaultSubobject<UArrowComponent>(TEXT("spawnPoint"));

	//spawnpoint->SetupAttachment(Gun);

	// ...
}


// Called when the game starts
void UGunComponent::BeginPlay()
{
	Super::BeginPlay();
	
	// ...
	
}


void UGunComponent::Fire_Implementation(AEnemyBase* enemy)
{
//FVector targetloc=	enemy->GetActorLocation();
//FVector  towertransform = GetOwner()->GetActorLocation();

//FVector Dir = towertransform - targetloc;

//FRotator rotate = UKismetMathLibrary::Conv_VectorToRotator(Dir);

//Gun->SetWorldRotation(rotate);
}

// Called every frame
void UGunComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

