// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyBase.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerDieSignaiture);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerKilledSignaiture,int, drop);
UCLASS()
class GMPRG2_P2_API AEnemyBase : public ACharacter
{
	GENERATED_BODY()




public:
	// Sets default values for this character's properties
	AEnemyBase();

	
	UFUNCTION()
	void die();
	void SetSalvagersActived(bool isActivated);
	bool GetSlavagerActivated();
	void OnBurn(float Damage,bool isHit,float Duration, float fireRate);
	UFUNCTION(BlueprintPure)
	float GetBaseHP();
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float CurrentSpeed;
	UPROPERTY( BlueprintReadWrite)
	bool slowEffects;
	void SetHP(float waveScale);
	UFUNCTION(BlueprintPure)
	float GetbaseSpeed();
	UFUNCTION(BlueprintImplementableEvent)
	void AddSlow(float slowRate);

	UFUNCTION(BlueprintImplementableEvent)
	void EndSlow();
bool IsBurning;
float BurnDuration;
UPROPERTY(BlueprintAssignable)
		FPlayerDieSignaiture OnEnemyDie;
UPROPERTY(BlueprintAssignable)
		FPlayerKilledSignaiture onKilled;
protected:
	UPROPERTY(EditAnywhere)
	float BaseSpeed;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadOnly);
	int32 Drop;
	void EndBurn();
	bool SalvaersActive;


	//float BurningDuration;
	void BurningEffect(bool stillHit, float Damage, float fireRate);
	

	bool end;

	//hp<=0;
	UFUNCTION()
	void killed();

	//if killed or reached base

UPROPERTY(EditAnywhere,BlueprintReadOnly);
	float BaseHP;
public:
	UPROPERTY(EditAnywhere, BluePrintReadOnly)
		class  UHPComponent* healthComponent;
	UFUNCTION( BlueprintCallable,BlueprintPure)
		int32 getHp();
	UFUNCTION(BluePrintCallable)
		void TakeDamage(int32 Damage);

//	UFUNCTION(Blueprint)
	//UPROPERTY(EditAnywhere)
	int32 currentWaypoint;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float getHealthPercentage();
	
//TArray<AActor*>Targetlist;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	bool getEnd();
	void enemyMoveTo();

	//void ememyAttack();

	void SetEnemyDrop(int32 scale);

	//void SetCurrentPoint(AActor*nextPoint);
	//void MoveToWaypoint
};
