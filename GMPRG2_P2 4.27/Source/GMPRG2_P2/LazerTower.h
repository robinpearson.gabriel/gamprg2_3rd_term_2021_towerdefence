// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "LazerTower.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API ALazerTower : public ATowerBase
{
	GENERATED_BODY()
protected:
		virtual void targetExit(AActor* target)override;
		virtual void getEnemy(AActor* element)override;
		virtual void Tick(float DeltaTime) override;
		void HitTarget();
		UFUNCTION(BlueprintImplementableEvent)
			void	DrawLine(FVector Startpos,FVector end);

		UPROPERTY(EditAnywhere)
			float damage;
		UPROPERTY(EditAnywhere)
		float duration;
		UPROPERTY(EditAnywhere)
		float fireRate;
		virtual void SetInformation()override;


		UPROPERTY(EditAnywhere)
		TArray<float> DamageIncrease;
		UPROPERTY(EditAnywhere)
		TArray<float> fireRateIncrease;
		virtual void Upgrade()override;

};


