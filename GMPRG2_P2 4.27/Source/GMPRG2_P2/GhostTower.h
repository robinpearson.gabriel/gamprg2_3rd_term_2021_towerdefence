// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GhostTower.generated.h"

UCLASS()
class GMPRG2_P2_API AGhostTower : public AActor
{

	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGhostTower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(BlueprintReadOnly)
	float Range;
	UFUNCTION(BlueprintImplementableEvent)
		void setRadius(bool onNode);
	

	void RangeIndicator(bool onUnoccupiedNode);
	class UTowerData* towerData;
public:	

void SetPos(FVector Location,bool isHit);
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* mesh;
	void SetupGhost(UTowerData* towerData);
};
