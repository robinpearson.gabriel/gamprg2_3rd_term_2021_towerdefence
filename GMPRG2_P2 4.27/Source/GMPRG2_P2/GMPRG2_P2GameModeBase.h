// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GMPRG2_P2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API AGMPRG2_P2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
