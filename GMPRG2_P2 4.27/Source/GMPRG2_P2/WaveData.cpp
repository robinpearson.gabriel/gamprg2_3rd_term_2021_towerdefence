// Fill out your copyright notice in the Description page of Project Settings.

#include "WaveData.h"

int32 UWaveData::GetDuration()
{
	return int32();
}

int32 UWaveData::GetTotalSpawn()
{
	int32 Total=0;
	for (FenemyToSpawn enemy: enemies)
	{
		Total += enemy.population;
	}

	return Total;
}
