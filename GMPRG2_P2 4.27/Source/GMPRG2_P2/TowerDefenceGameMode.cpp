// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerDefenceGameMode.h"
#include "Base.h"
#include "WaveData.h"
#include "Spawner.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyBase.h"


int32 ATowerDefenceGameMode::GetHP()
{
	return HP;
}

void ATowerDefenceGameMode::BeginPlay()
{
	HP = 20;

	Money = 300;
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABase::StaticClass(), Bases);
	for (AActor* obj :Bases)
	{
		if (ABase* base = Cast<ABase>(obj))
		{
			
			base->onBaseHit.AddDynamic(this, &ATowerDefenceGameMode::TakeDamage);
		}
	}
	
	GetSpawners();
	CurrentwaveIndex = 0;
	WaveStart(wavedatas[CurrentwaveIndex]);
}

void ATowerDefenceGameMode::WaveStart(UWaveData* wavedata)
{
	totalWaveEnemy=wavedata->GetTotalSpawn();
	EnemyLeft = totalWaveEnemy;


	//int num = wavedata->enemies.Num();
	
	for (AActor* act : Spawners)
	{
		if (ASpawner* spawner = Cast<ASpawner>(act))
		{
			spawner->index = 0;
			spawner->HpScaling = wavedata->EnemyHpScaling;
			spawner->DropScaling=wavedata->DropRateScale;
			spawner->ClearEnemyToSPawn();
		}
	}
	
	for (int x=0; x < wavedata->enemies.Num(); x++)
	{
		if (ASpawner* spawner = Cast<ASpawner>(GetEnemySpawner(wavedata->enemies[x].Spawner)))
		{

			spawner->SetCanSpawn(true);
				
				spawner->setEnemytoSpawn(wavedata->enemies[x].enemyClass, wavedata->enemies[x].population);			
		}
	}


	
}


AActor* ATowerDefenceGameMode::GetEnemySpawner(int index)
{
	index = FMath::Clamp(index, 0, (Spawners.Num()-1));
	return Spawners[index];
}



void ATowerDefenceGameMode::GetSpawners()
{
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawner::StaticClass(), Spawners);

	for (AActor* spawner : Spawners)
	{
		if (ASpawner* spawn = Cast<ASpawner>(spawner))
		{
			spawn->setGameMode(this);

		}

	}
	
}







float ATowerDefenceGameMode::GetwaveProgression()
{

	totalWaveEnemy= FMath::Max(totalWaveEnemy,1);

	return ((float)totalWaveEnemy-(float)EnemyLeft)/(float)totalWaveEnemy;
}

int32 ATowerDefenceGameMode::gettoTalWaves()
{
	return wavedatas.Num();
}

int32 ATowerDefenceGameMode::getCurrentWaveIndex()
{
	return CurrentwaveIndex;
}

void ATowerDefenceGameMode::TakeDamage()
{

	
	if (HP > 0)
	{ HP--;
	if (HP <= 0)
	{
		Lose();
	}
	}

}

void ATowerDefenceGameMode::enemyDied()
{

	 
	EnemyLeft--;
	if (GetwaveProgression() >= 1)
	{
		nextwave();
	}
}

int32 ATowerDefenceGameMode::GetMoney()
{
	return Money;
}

void ATowerDefenceGameMode::IncreaseMoney(int32 increase)
{
	Money += increase;
}

void ATowerDefenceGameMode::Buy(int32 Decrease)
{
	Money -= Decrease;
}

void ATowerDefenceGameMode::nextwave()
{
	if (HP > 0)
	{
		if (CurrentwaveIndex + 1 < gettoTalWaves())
		{
			IncreaseMoney(wavedatas[CurrentwaveIndex]->WaveGold);

			totalWaveEnemy = 0;
			EnemyLeft = 0;
			CurrentwaveIndex++;



			WaveStart(wavedatas[CurrentwaveIndex]);
		}
		else
		{
			Win();

		}
	}
}

void ATowerDefenceGameMode::BindtoEnemy(class AEnemyBase* enemy)
{
	enemy->OnEnemyDie.AddDynamic(this, &ATowerDefenceGameMode::enemyDied);
	enemy->onKilled.AddDynamic(this, &ATowerDefenceGameMode::IncreaseMoney);
}
