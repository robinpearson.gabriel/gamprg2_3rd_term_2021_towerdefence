// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "turretBullet.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API AturretBullet : public AProjectile
{
	GENERATED_BODY()
protected:
		virtual void hit(AActor* enemy)override;
};
