// Fill out your copyright notice in the Description page of Project Settings.
#include "Spawner.h"
#include "Targets.h"
#include "EnemyBase.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "TowerDefenceGameMode.h"
#include "MyAIController.h"

void ASpawner::ClearEnemyToSPawn()
{
	enemyToSpawn.Empty();
}

void ASpawner::SetCanSpawn(bool n)
{
	CanSpawn = n;
}

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ASpawner::setEnemytoSpawn(TSubclassOf <class AEnemyBase> enemywavedata,int32 population)
{
	//bool hasStarted= enemyToSpawn.Num() > 0;
	
	for (int i = population; i > 0; i--)
	{

		enemyToSpawn.Add(enemywavedata);

	}
	

	if(CanSpawn)
	{ 
		
	SetCanSpawn(false);
		SpawnEnemy(3);
		
	}
		
	
	

}

void ASpawner::setGameMode(class ATowerDefenceGameMode* gamemode)
{
	gameMode = gamemode;
}



// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	
	CanSpawn = true;
	Super::BeginPlay();
	//Spawnpoint = topRoute[0];
	//SpawnEnemy(0);
	ClearEnemyToSPawn();
}
void ASpawner::SpawnEnemy(int32 duration)
{
	if (index < enemyToSpawn.Num())
	{
		//if (GEngine)
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::FromInt(index));
		//SetCanSpawn();
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.bNoFail = true;
		SpawnParams.Owner = this;
		//SpawnParams.Instigator = this;

		FTransform pos;
		//FVector location = 
		pos.SetLocation(Spawnpoint->GetActorLocation());
		pos.SetRotation(Spawnpoint->GetActorRotation().Quaternion());

		//AObstacle* obstacle = Cast<AObstacle>(GetWorld()->SpawnActor<AObstacle>(ObstacleClasses[rand], pos, SpawnParams));
		//AEnemyBase
		AEnemyBase* enemytoSpawna = Cast<AEnemyBase>(GetWorld()->SpawnActor<AEnemyBase>(enemyToSpawn[index], pos, SpawnParams));
		if (AMyAIController* controller = Cast<AMyAIController>(enemytoSpawna->GetController()))
		{
			controller->SetRoute(Route);
			controller->EnemyMove();
		}
		gameMode->BindtoEnemy(enemytoSpawna);
		enemytoSpawna->SetHP(HpScaling);
		enemytoSpawna->SetEnemyDrop(DropScaling);
		index++;
		if (index < enemyToSpawn.Num())
		{
			FTimerDelegate RespawnDelegate = FTimerDelegate::CreateUObject(this, &ASpawner::SpawnEnemy, 3);
			//	GetWorldTimerManager().SetTimer(TimerHanle, this, RespawnDelegate, 1.0f, false, 1.0f);
			GetWorldTimerManager().SetTimer(TimerHanle, RespawnDelegate, duration, false);
		}
		}
	else 
	{
		//enemyToSpawn.Empty();
		SetCanSpawn(true);
	}
	
}


// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void spawnChar()
{

}

