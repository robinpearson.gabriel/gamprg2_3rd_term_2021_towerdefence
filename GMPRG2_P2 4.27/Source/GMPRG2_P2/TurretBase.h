// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "TurretBase.generated.h"

/**
 * 
 */

UCLASS()
class GMPRG2_P2_API ATurretBase : public ATowerBase
{
	GENERATED_BODY()

public:
		
private:
	FTimerHandle TimerHanler;

protected:
	
	UPROPERTY(EditAnywhere, BluePrintReadOnly)
	float Damage;
	void Shoot();
	UPROPERTY(EditAnywhere, BluePrintReadOnly)
	TSubclassOf<class AProjectile> bulletType;
	
	float basefireRate;

	UPROPERTY(EditAnywhere, BluePrintReadOnly)
	float fireRate;


	virtual void getEnemy(AActor* enemy) override;
public:
	ATurretBase();

		virtual void Tick(float DeltaTime) override;
	
		virtual void Overloaded(float Bonus)override;
		virtual void UndoOverload()override;
		virtual void SetInformation()override;
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> DamageIncrease;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> fireRateIncrease;


		virtual void Upgrade()override;
};
