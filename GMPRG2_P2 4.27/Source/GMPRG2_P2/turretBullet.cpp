// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyBase.h"
#include "turretBullet.h"

void AturretBullet::hit(AActor* enemy)
{
	if (AEnemyBase* Enemy = Cast<AEnemyBase>(enemy))
	{
		Enemy->TakeDamage(GetDamage());

		this->Destroy();
	}
}