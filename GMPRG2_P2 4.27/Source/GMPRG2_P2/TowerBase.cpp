// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerBase.h"
#include "EnemyBase.h"
#include "Engine/StaticMesh.h"
#include "Components/ArrowComponent.h"
#include "TowerDefenceGameMode.h"
#include "TowerNodes.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"
#include "Engine/StaticMesh.h"
#include "Kismet/KismetMathLibrary.h"
// Sets default values
ATowerBase::ATowerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	root = CreateDefaultSubobject<USceneComponent>(TEXT("root"));
	collider = CreateDefaultSubobject<USphereComponent>(TEXT("Collider"));
	Gun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("gun"));
	GunBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gun Base"));
	pivot = CreateDefaultSubobject<UArrowComponent>(TEXT("Pivot"));
	spawnpoint = CreateDefaultSubobject<UArrowComponent>(TEXT("spawnPoint"));
	
	pivot->SetupAttachment(root);
	spawnpoint->SetupAttachment(pivot);

	collider->SetupAttachment(root);
	Gun->SetupAttachment(pivot);
	
	GunBase->SetupAttachment(collider);
	isOverloaded = false;
	Radius = 1000;
	collider->InitSphereRadius(Radius );
}

// Called when the game starts or when spawned
void ATowerBase::BeginPlay()
{
	Super::BeginPlay();
	//this->collider->SetWorldScale3D(FVector(Radius));
	//collider->InitSphereRadius(Radius*collider->GetScaledSphereRadius());
	collider->OnComponentBeginOverlap.AddDynamic(this, &ATowerBase::OnActorOverlap);

	collider->OnComponentEndOverlap.AddDynamic(this, &ATowerBase::OnActorExit);

	level = 0;
	Upgrade();
	
}

FTransform ATowerBase::GetPivotTransform()
{
	return pivot->GetComponentTransform();
}

void ATowerBase::SellTower()
{
	if (ATowerDefenceGameMode* GameMode = Cast<ATowerDefenceGameMode>(GetWorld()->GetAuthGameMode()))
	{
		GameMode->IncreaseMoney(SellCost);
	}
	
	node->RemoveTower();
	this->Destroy();
}

void ATowerBase::Upgrade()
{
	if (level > 0)
	{
		IncreaseSellValue(Cost[level]);
		
	}

	level++;

	
}

bool ATowerBase::GetCanUpgrade(int32 money)
{
	if (level < 3)
	{
		if (money >= Cost[level - 1])
		{

			return true;
		}
		else
		{
			return false;
		}
	}
	else { return false; }
}

void ATowerBase::getEnemy(AActor* element)
{
	
}

void ATowerBase::targetExit(AActor* element)
{
}


void ATowerBase::Fire_Implementation(AActor* enemy)
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("en"));

	FVector targetloc = enemy->GetActorLocation();
	//FVector  towertransform = this->GetActorLocation();
	FVector towertransform = pivot->GetComponentLocation();


	FVector Dir =   targetloc-towertransform;

	FRotator rotate = UKismetMathLibrary::Conv_VectorToRotator(Dir);

	pivot->SetWorldRotation(rotate);

}

void ATowerBase::OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
//if(AEnemyBase* enemy = Cast<AEnemyBase>(OtherActor))
//{ 
	//targets.Add(enemy);

//	getTargets(OtherActor);
	getEnemy(OtherActor);
//}
	
}

void ATowerBase::OnActorExit( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
	RemoveFromList(OtherActor);
	
}

// Called every frame
void ATowerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
		
		
			
		
		
	




}

void ATowerBase::Overloaded(float Bonus)
{
	isOverloaded = true;
}

void ATowerBase::UndoOverload()
{
	isOverloaded = false;
}

bool ATowerBase::GetOverloaded()
{
	return isOverloaded;
}

void ATowerBase::IncreaseSellValue(int32 value)
{
	SellCost += (value / 2);

}

void ATowerBase::RemoveFromList(AActor* obj)
{	targetExit(obj);
	if (targets.Contains(obj))
	{
	
		targets.Remove(obj);
		
		if (targets.Num() >= 1)
		{
			target = targets[0];
		}
		else
		{
			target = NULL;

		}
	}
}

void ATowerBase::SetInformation()
{
	
}

int32 ATowerBase::getUpgradeCost()
{
	if (level<Cost.Num())
	{
return Cost[level];
	}
	else
	{
		return 0;
	}
	
}

void ATowerBase::SetNode(ATowerNodes* Node)
{
	node = Node;
}


