// Fill out your copyright notice in the Description page of Project Settings.
#include "Components/StaticMeshComponent.h"
#include "TowerData.h"
#include "GhostTower.h"
#include "DrawDebugHelpers.h"
// Sets default values
AGhostTower::AGhostTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("meshBody"));
}

// Called when the game starts or when spawned
void AGhostTower::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGhostTower::SetPos(FVector Location, bool isHit)
{SetActorLocation(Location);
	if (isHit)
	{
		
		//DrawDebugSphere(GetWorld(), GetActorLocation(), Range, 20, FColor::Green, false, 0.1f);
	}
	else
	{
		//DrawDebugSphere(GetWorld(), GetActorLocation(), Range, 20, FColor::Red, false, 0.1f);
	}
	setRadius(isHit);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("World delta for current frame equals %f"), GetWorld()->TimeSeconds));
}

// Called every frame
void AGhostTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGhostTower::SetupGhost(UTowerData* TowerData)
{
	 towerData= TowerData;
	 mesh->SetStaticMesh ( towerData->mesh);
	 Range = towerData->range;
}

