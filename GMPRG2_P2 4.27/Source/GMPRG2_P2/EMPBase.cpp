// Fill out your copyright notice in the Description page of Project Settings.


#include "EMPBase.h"
#include "EnemyBase.h"
void AEMPBase::targetExit(AActor* actor)
{
	Super::targetExit(actor);
	if (AEnemyBase* enem = Cast<AEnemyBase>(actor))
	{
		
		enem->EndSlow();

	}
}

void AEMPBase::getEnemy(AActor* element)
{
	Super::getEnemy(element);
	if (AEnemyBase* enem = Cast<AEnemyBase>(element))
	{targets.AddUnique(enem);
	//slowed
	if (!enem->slowEffects)
	{
		enem->AddSlow(SlowEffect);
	}
	}
}

void AEMPBase::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle TimerHandler;
	
	GetWorldTimerManager().SetTimer(TimerHandler, this, &AEMPBase::CheckAgain, 1.0f, true);
}

void AEMPBase::CheckAgain()
{
	if (targets.Num() > 0)
	{
		for (int x = 0; x < targets.Num(); x++)
		{
			if (AEnemyBase* enemy = Cast<AEnemyBase>(targets[x]))
			{
				if (!enemy->slowEffects)
				{
					enemy->AddSlow(SlowEffect);

				}

			}
		}
	}
}

void AEMPBase::SetInformation()
{
	Super::SetInformation();

	

	Name = "EMP BASE";

	Stats = "Radius :" + FString::FromInt(Radius);
	Stats += "\n";
	Stats += "SlowEffect :" + FString::SanitizeFloat(SlowEffect);
}

void AEMPBase::Upgrade()
{
	Super::Upgrade();
	Radius = RangeIncreas[level-1 ];
	SlowEffect = slowIncrease[level-1];
	Gun->SetStaticMesh(meshes[level-1]);

}
