// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TimerManager.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class GMPRG2_P2_API ASpawner : public AActor
{
	GENERATED_BODY()
	
private:
	FTimerHandle TimerHanle;
	bool CanSpawn;
class ATowerDefenceGameMode* gameMode;

public:	
	// Sets default values for this actor's properties
	ASpawner();

	float HpScaling;
	float DropScaling;
	void ClearEnemyToSPawn();
	
	void SetCanSpawn(bool n );
	int32 index;

	void setEnemytoSpawn(TSubclassOf<class AEnemyBase> enemywavedata, int32 population);
	void setGameMode(class ATowerDefenceGameMode* gamemode);
	


	void SpawnEnemy(int32 index);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	
	UPROPERTY(EditAnywhere, BluePrintReadOnly)
		TArray<AActor*> Route;

	

	UPROPERTY(EditAnywhere, BluePrintReadOnly)
		AActor* Spawnpoint;


	UPROPERTY(BluePrintReadOnly)
	TArray	<TSubclassOf <class AEnemyBase>>enemyToSpawn;
	 
	
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



};
