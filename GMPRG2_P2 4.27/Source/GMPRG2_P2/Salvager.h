// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "Salvager.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API ASalvager : public ATowerBase
{
	GENERATED_BODY()
protected:
	virtual void targetExit(AActor* target)override;
	virtual void getEnemy(AActor* element)override;
	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;
	
	void miningCoin();
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float dropIncreaseRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int coinPerMine;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<int> coinPerMineIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<float> dropIncreaseIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<float> RangeIncrease;
	UFUNCTION()
	void IncreaseDrop(int Drop);
	
	void CheckAgain();
	virtual void SetInformation()override;
	virtual void Upgrade()override;
};
