// Fill out your copyright notice in the Description page of Project Settings.
#include "TowerBase.h"
#include "Components/ArrowComponent.h"
#include "TowerNodes.h"

// Sets default values
ATowerNodes::ATowerNodes()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BuildPos = CreateDefaultSubobject<UArrowComponent>(TEXT("Build Position"));
	IsOcuppied = false;
}

bool ATowerNodes::GetIsOccupied()
{
	return IsOcuppied;
}

// Called when the game starts or when spawned
void ATowerNodes::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATowerNodes::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATowerNodes::buildTower(TSubclassOf<class ATowerBase> TowerClass,int32 value)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.bNoFail = true;
	SpawnParams.Owner = this;
	//SpawnParams.Instigator = this;

	FTransform pos;
	//FVector location = 
	pos.SetLocation(BuildPos->GetComponentLocation());
	pos.SetRotation(BuildPos->GetComponentRotation().Quaternion());

	//AObstacle* obstacle = Cast<AObstacle>(GetWorld()->SpawnActor<AObstacle>(ObstacleClasses[rand], pos, SpawnParams));
	//AEnemyBase
	ATowerBase* tower = Cast<ATowerBase>(GetWorld()->SpawnActor<ATowerBase>(TowerClass, pos, SpawnParams));
	towerBuilt = tower;
	towerBuilt->SetNode(this);
	towerBuilt->IncreaseSellValue(value);
	IsOcuppied = true;
}

void ATowerNodes::RemoveTower()
{
	towerBuilt = nullptr;
	IsOcuppied = false;
}

