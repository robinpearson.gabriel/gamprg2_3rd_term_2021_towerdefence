// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MouseController.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(F_PressedSigniture, AActor*, HitObj, FVector,Pos);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(F_RealeasedSigniture);
UCLASS()
class GMPRG2_P2_API AMouseController : public APlayerController
{
	GENERATED_BODY()
public:	
		UPROPERTY(BlueprintAssignable)
		F_PressedSigniture onMouseHit;

	UPROPERTY(BlueprintAssignable)
		F_RealeasedSigniture onMouseReleased;

protected:
virtual void BeginPlay() override;
bool IsActive;
public:
	
	UFUNCTION(BlueprintCallable)
	void	Activated();

	AMouseController();
	virtual void Tick(float DeltaTime) override;
		void mouse();
		UFUNCTION()
		void clicked();
		UFUNCTION()
		void released();

		virtual void SetupInputComponent() override;
};
