// Fill out your copyright notice in the Description page of Project Settings.


#include "Salvager.h"
#include "TowerDefenceGameMode.h"
#include "EnemyBase.h"
void ASalvager::targetExit(AActor* act)
{
	Super::targetExit(act);
	if (AEnemyBase* enem = Cast<AEnemyBase>(act))
	{ 
		
		enem->onKilled.RemoveDynamic(this, &ASalvager::IncreaseDrop);
		enem->SetSalvagersActived(false);
	}

}

void ASalvager::getEnemy(AActor* element)
{
	Super::getEnemy(element);
	if (AEnemyBase* enem = Cast<AEnemyBase>(element))
	{
		targets.AddUnique(enem);
		if (!enem->GetSlavagerActivated())
		{
			enem->onKilled.AddDynamic(this, &ASalvager::IncreaseDrop);
			enem->SetSalvagersActived(true);

		}
	}

}

void ASalvager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASalvager::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle TimerHandler;
	FTimerHandle TimerHandlerA;
	GetWorldTimerManager().SetTimer(TimerHandler,this, &ASalvager::miningCoin, 3.0f, true);
	GetWorldTimerManager().SetTimer(TimerHandlerA, this, &ASalvager::CheckAgain, 1.0f, true);

}

void ASalvager::miningCoin()
{
	if (ATowerDefenceGameMode* GameMode = Cast<ATowerDefenceGameMode>(GetWorld()->GetAuthGameMode()))
	{
		GameMode->IncreaseMoney(coinPerMine);

	}
}

void ASalvager::IncreaseDrop(int Drop)
{
	 if (ATowerDefenceGameMode* GameMode = Cast<ATowerDefenceGameMode>(GetWorld()->GetAuthGameMode()))
	{
			GameMode->IncreaseMoney( Drop*dropIncreaseRate);
			GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::White, FString::Printf(TEXT("hello")));
	}
}

void ASalvager::CheckAgain()
{
	if (targets.Num() > 0)
	{
		for  (int x=0; x<targets.Num();x++)
		{
			if (AEnemyBase* enemy = Cast<AEnemyBase>(targets[x]))
			{

				if (!enemy->GetSlavagerActivated())
				{
					enemy->onKilled.AddDynamic(this, &ASalvager::IncreaseDrop);
					enemy->SetSalvagersActived(true);

				}
			}
		}
	}
}

void ASalvager::SetInformation()
{
	Super::SetInformation();
	Name = "Salvager";
	Stats = "Radius :" + FString::FromInt(Radius)+"\n"+"Gold Per 3s :"+FString::FromInt(coinPerMine)+"\n"+"Drop Increase :" +FString::SanitizeFloat(dropIncreaseRate);
}

void ASalvager::Upgrade()
{
	Super::Upgrade();
	Radius = RangeIncrease[level - 1];
	coinPerMine = coinPerMineIncrease[level - 1];
	dropIncreaseRate = dropIncreaseIncrease[level - 1];
	Gun->SetStaticMesh(meshes[level-1]);
}
