// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MyAIController.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API AMyAIController : public AAIController
{
	GENERATED_BODY()
		void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)override;

protected:
	TArray<AActor*> route;
	
public:

	void SetRoute(TArray<AActor*> waypoints);
	void EnemyMove();
	//int32 Currentwaypoint;
};
