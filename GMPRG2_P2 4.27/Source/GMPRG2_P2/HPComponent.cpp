// Fill out your copyright notice in the Description page of Project Settings.


#include "HPComponent.h"

// Sets default values for this component's properties
UHPComponent::UHPComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UHPComponent::GetHP()
{
	return HP;
}

float UHPComponent::GetHPPercentage()
{
	return HP/MaxHp;
}

void UHPComponent::SetHealth(float baseHealth, float wavescaling)
{
	MaxHp = baseHealth * wavescaling;
	HP = MaxHp;
}

void UHPComponent::TakeDamage(float Damage)
{
	HP -= Damage;
}

// Called when the game starts
void UHPComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHPComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

