// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBase.h"
#include "Projectile.h"
#include "MissileTowerBase.h"

void AMissileTowerBase::Shoot()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.bNoFail = true;
	SpawnParams.Owner = this;
	//SpawnParams.Instigator = this;

	FTransform pos;


	pos = GetPivotTransform();
	if (AProjectile* bullet = Cast<AProjectile>(GetWorld()->SpawnActor<AProjectile>(bulletType, pos, SpawnParams)))
	{
		bullet->SetDamage(Damage);
		bullet->setexplosionRadius(ExplotionRadius);
	}

}


void AMissileTowerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!target->IsValidLowLevel())
	{
		RemoveFromList(target);
		if (targets.Num() > 0)
		{
			target = targets[0];

		}

	}
	else
	{

		Fire(target);
		Shoot();
		//GetWorldTimerManager().SetTimer(TimerHanler,this, &ATurretBase::Shoot, 1.0f, true);
	}



}

void AMissileTowerBase::Overloaded(float Bonus)
{
	Super::Overloaded(Bonus);
	fireRate *= Bonus;
}

void AMissileTowerBase::UndoOverload()
{
	Super::UndoOverload();
	fireRate = BasefireRate;
}

void AMissileTowerBase::SetInformation()
{
	Super::SetInformation();
	Name = "Missile Tower";
	Stats = "Radius :"+FString::FromInt(Radius)+"\n"+"Fire Rate :"+FString::SanitizeFloat( fireRate)+"\n"+"Damage :"+FString::SanitizeFloat(Damage);
}

void AMissileTowerBase::Upgrade()
{
	Super::Upgrade();
	fireRate = fireRateIncrease[level - 1];
	Damage = DamageIncrease[level - 1];
	ExplotionRadius = explosionRadiusIncrease[level - 1];
	Gun->SetStaticMesh(meshes[level - 1]);


}

void AMissileTowerBase::getEnemy(AActor* enemy)
{
	Super::getEnemy(enemy);

	if (AEnemyBase* targ = Cast<AEnemyBase>(enemy))

	{
		targets.AddUnique(enemy);
		//if (!target->IsValidLowLevelFast())
		//{
		//	target=targets[0];
		//}
	}
}

void AMissileTowerBase::BeginPlay()
{
	Super::BeginPlay();
	BasefireRate = fireRate;
}
