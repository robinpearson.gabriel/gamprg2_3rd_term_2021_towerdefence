// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

UCLASS()
class GMPRG2_P2_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere)
	TSubclassOf<class AGhostTower> GhostTowerPrefab;

	class UTowerData* TowerData;

	UFUNCTION(BlueprintCallable)
	void SpawnGhost(UTowerData* data);
	UFUNCTION()
	void CheckNode(AActor* actor,FVector pos);
	UFUNCTION()
	void EndChecking();


	UFUNCTION()
		void OpenTowerHUD();

	UFUNCTION()
	void checkTowers(AActor* actor, FVector pos);

	class AGhostTower* Ghost;

	class ATowerBase* OpenTower;
	class ATowerBase* HitTower;
	bool canOpen;
	class ATowerNodes* node;
	bool canBuild;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
