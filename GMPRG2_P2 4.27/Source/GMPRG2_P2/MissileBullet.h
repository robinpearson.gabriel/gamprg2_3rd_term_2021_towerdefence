// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "MissileBullet.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_P2_API AMissileBullet : public AProjectile
{
	GENERATED_BODY()
protected:
	virtual void hit(AActor* enemy)override;

	UFUNCTION(BlueprintImplementableEvent)
		void hitsomething();
public:
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float Radius;

	virtual void setexplosionRadius(float radius)override;
};
