// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBase.h"
#include "MyAIController.h"
#include "Targets.h"
#include "Kismet/GameplayStatics.h"
#include "HPComponent.h"
#include "TowerDefenceGameMode.h"


// Sets default values
AEnemyBase::AEnemyBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	

	healthComponent= CreateDefaultSubobject<UHPComponent>(TEXT("Health"));
}



// Called when the game starts or when spawned
void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();
	currentWaypoint = 1;
//	UGameplayStatics::GetAllActorsOfClass(GetWorld(),ATargets::StaticClass(), Targetlist);
//	enemyMoveTo();
//	end = false;
	SalvaersActive = false;
	slowEffects = false;
	IsBurning = false;
}

void AEnemyBase::EndBurn()
{
	if (BurnDuration <= 0)
	{
		IsBurning = false;
	}
	else
	{
		FTimerHandle TimerHandlerC;
		BurnDuration--;
		GetWorldTimerManager().SetTimer(TimerHandlerC, this, &AEnemyBase::EndBurn, 1.0f, false);
	}
}

void AEnemyBase::BurningEffect(bool stillHit,float Damage,float fireRate)
{
	TakeDamage(Damage);
	if (stillHit)
	{
		FTimerHandle TimerHandlerA;
		FTimerDelegate Burning = FTimerDelegate::CreateUObject(this, &AEnemyBase::BurningEffect, IsBurning, Damage,fireRate);
		
		GetWorldTimerManager().SetTimer(TimerHandlerA, Burning, fireRate, false);
	}
}




void AEnemyBase::killed()
{
	//if (ATowerDefenceGameMode* GameMode = Cast<ATowerDefenceGameMode>(GetWorld()->GetAuthGameMode()))
	//{
	//	GameMode->IncreaseMoney(Drop);
	//}
	onKilled.Broadcast(Drop);
	die();

}

void AEnemyBase::die()
{
	OnEnemyDie.Broadcast();
	//if (ATowerDefenceGameMode* GameMode = Cast<ATowerDefenceGameMode>(GetWorld()->GetAuthGameMode()))
	//{
	//	GameMode->enemyDied();
	//}
	this->Destroy();
	
}

void AEnemyBase::SetSalvagersActived(bool isActivated)
{
	SalvaersActive = isActivated;
}

bool AEnemyBase::GetSlavagerActivated()
{
	return SalvaersActive;
}

void AEnemyBase::OnBurn(float Damage, bool isHit, float Duration, float fireRate)
{


	if (isHit)
	{
		BurnDuration = Duration;
		if (!IsBurning)
		{
			IsBurning = true;
			FTimerHandle TimerHandlerA;
			FTimerDelegate Burning = FTimerDelegate::CreateUObject(this, &AEnemyBase::BurningEffect, IsBurning, Damage,  fireRate);
			TakeDamage(Damage);

			GetWorldTimerManager().SetTimer(TimerHandlerA, Burning, fireRate, false);
		}
	}
	else
	{
		FTimerHandle TimerHandlerB;

		GetWorldTimerManager().SetTimer(TimerHandlerB,this, &AEnemyBase::EndBurn, 1.0f, false);

	}
}

float AEnemyBase::GetBaseHP()
{
	return BaseHP;
}

void AEnemyBase::SetHP(float waveScale)
{
	healthComponent->SetHealth(BaseHP,waveScale);
}

float AEnemyBase::GetbaseSpeed()
{
	return BaseSpeed;
}

int32 AEnemyBase::getHp()
{
	return healthComponent->GetHP();
}

void AEnemyBase::TakeDamage(int32 Damage)
{
	healthComponent->TakeDamage(Damage);

	if (healthComponent->GetHP() <= 0)
	{
		killed();

	}
}

float AEnemyBase::getHealthPercentage()
{
	return healthComponent->GetHPPercentage();
}

// Called every frame
void AEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void AEnemyBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool AEnemyBase::getEnd()
{

	return end;
}

void AEnemyBase::enemyMoveTo()
{
	
		if (AMyAIController* controller = Cast<AMyAIController>(this->GetController()))
		{

			controller->EnemyMove();
			//SetCurrentPoint(currentWaypoint->GetNextTarget(this->Route));

			/*if (currentWaypoint <= Targetlist.Num())
			{
				for (AActor* target : Targetlist)
				{
					if (ATargets* targetItr = Cast<ATargets>(target))
					{

						if (targetItr->GettargetOrderNum() == currentWaypoint)
						{
							if (GEngine)
								GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));

							currentWaypoint++;
							controller->MoveToActor(targetItr, 1.0f, false);


							break;
						}
					}
				}*/

				//}
				//else
				//{
				//	ememyAttack();
				//
				//}

		}
	
}
void AEnemyBase::SetEnemyDrop(int32 scale)
{
	Drop*=scale;
}
//
//void AEnemyBase::ememyAttack()
//{
//}



//void AEnemyBase::SetCurrentPoint(AActor*nextPoint)
//{
//
//	if (nextPoint != currentWaypoint && nextPoint!=NULL)
//	{
//		if (ATargets* target = Cast<ATargets>(nextPoint))
//		{
//			currentWaypoint = target;
//		}
//	}
//	else
//	{
//		end = true;
//	}
//}

