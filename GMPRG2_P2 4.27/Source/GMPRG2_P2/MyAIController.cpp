// Fill out your copyright notice in the Description page of Project Settings.


#include "MyAIController.h"
#include "EnemyBase.h"
void AMyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	

	


		EnemyMove();

}

void AMyAIController::SetRoute(TArray<AActor*> waypoints)
{
	route = waypoints;
}

void AMyAIController::EnemyMove()
{
	if (AEnemyBase* enemy = Cast<AEnemyBase>(GetPawn()))
	{
		if (route[enemy->currentWaypoint])
		{
			MoveToActor(route[enemy->currentWaypoint], 1.0f, false);
			enemy->currentWaypoint++;
		}

	}
	
}
