// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "EnemyBase.h"
#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"


/**
 * 
 */


USTRUCT(BlueprintType)
struct FenemyToSpawn
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf < AEnemyBase> enemyClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float population;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float Spawner;
		
};


UCLASS(BlueprintType)
class GMPRG2_P2_API UWaveData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Duration;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Spawns;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float EnemyHpScaling;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float DropRateScale;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float WaveGold;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FenemyToSpawn> enemies;

	UFUNCTION(BlueprintPure, BlueprintCallable)
		int32 GetDuration();


	int32 GetTotalSpawn();
};
