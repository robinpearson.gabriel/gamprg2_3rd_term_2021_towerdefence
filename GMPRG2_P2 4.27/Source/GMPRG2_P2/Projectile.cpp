// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "EnemyBase.h"
#include "Components/SphereComponent.h"
// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	body = CreateDefaultSubobject<USphereComponent>(TEXT("Body"));
	physics = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("movement"));
	physics->UpdatedComponent = body;

}

void AProjectile::SetTarget(AActor* enemy)
{
	target = enemy;
	 
	//if (AEnemyBase* targeted = Cast<AEnemyBase>(enemy))
//	{
		//if(targeted!=NULL)
		//physics->HomingTargetComponent = targeted->GetCapsuleComponent();
//	}
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	body->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnEnemyOverlap);
}

void AProjectile::OnEnemyOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	hit(OtherActor);
	OnHit(OtherActor);
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::SetDamage(float Dmg)
{
	Damage = Dmg;
}

void AProjectile::OnHit_Implementation(AActor* Enemy)
{
	
}

void AProjectile::hit(AActor* enemy)
{
}

float AProjectile::GetDamage()
{
	return Damage;
}

void AProjectile::setexplosionRadius(float radius)
{

}

