// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerGeneratorBase.h"
#include "TowerBase.h"
void APowerGeneratorBase::targetExit(AActor* targ)
{
	Super::targetExit(targ);
	if (ATowerBase* tower = Cast<ATowerBase>(targ))
	{
		targets.AddUnique(tower);
		if (!tower->GetOverloaded())
		{
		
		tower->UndoOverload();
		}


	}
}

void APowerGeneratorBase::getEnemy(AActor* element)
{
	Super::getEnemy(element);

	if (ATowerBase* tower = Cast<ATowerBase>(element))
	{
		targets.AddUnique(tower);
		if (!tower->GetOverloaded())
		{
		
		tower->Overloaded(Bonus);
		}


	}
}

void APowerGeneratorBase::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle TimerHandlerA;
	
	GetWorldTimerManager().SetTimer(TimerHandlerA, this, &APowerGeneratorBase::CheckAgain, 1.0f, true);
}

void APowerGeneratorBase::SetInformation()
{
	Super::SetInformation();
	Name = "Power Generator";
	Stats = "Radius :" + FString::FromInt(Radius) + "\n" + "Buff Attack Speed :" + FString::SanitizeFloat(Bonus);
}

void APowerGeneratorBase::Upgrade()
{
	Super::Upgrade();
	Bonus = BonusIncrease[level - 1];
	Radius = RadiusIncrease[level - 1];
	Gun->SetStaticMesh(meshes[level - 1]);
}

void APowerGeneratorBase::CheckAgain()
{
	if (targets.Num() > 0)
	{
		for (int x = 0; x < targets.Num(); x++)
		{

			if (ATowerBase* tower = Cast<ATowerBase>(targets[x]))
			{
				targets.AddUnique(tower);
				if (!tower->GetOverloaded())
				{

					tower->Overloaded(Bonus);
				}


			}
		}
	}
}

