// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerBase.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class GMPRG2_P2_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
	
public:
		UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<ATowerBase> TowerClass;
		UPROPERTY(EditAnywhere, BlueprintReadOnly)
			UStaticMesh* mesh;
		UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 Cost;
		UPROPERTY(EditAnywhere, BlueprintReadOnly)
			FString Name;
		UPROPERTY(EditAnywhere, BlueprintReadOnly)
			float range;
		


		

};
