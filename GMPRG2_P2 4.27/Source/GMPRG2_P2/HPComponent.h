// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HPComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GMPRG2_P2_API UHPComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHPComponent();

	float HP;
	float MaxHp;
	UFUNCTION(BlueprintPure)
		float	GetHP();

	UFUNCTION(BlueprintPure)
		float	GetHPPercentage();

	void SetHealth(float baseHealth, float wavescaling);

	void TakeDamage(float Damage);
	
	
	
	

protected:
	// Called when the game starts

	//int32 EnemyHP;
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
